<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<?php
$container   = get_theme_mod( 'beplustheme_container_type' );
$archiveTitle = stripslashes( get_option( get_post_type() . '_archive_title' ) );
$archiveDescription = stripslashes( get_option( get_post_type() . '_archive_description' ) );
?>

<div class="wrapper" id="site-archive-wrapper">

	<div id="site-archive">

		<div id="site-archive-container">

			<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

				<div class="row">
					
					<?php if( $archiveTitle || $archiveDescription ) : ?>
						<header class="bps-archive-page-header">
							<?php if( $archiveTitle ) : ?>
								<h1 class="bps-archive-page-title"><?php echo $archiveTitle; ?></h1>
							<?php endif; ?>
							<?php if( $archiveDescription ) : ?>
								<h5 class="bps-archive-page-description"><?php echo $archiveDescription; ?></h5>
							<?php endif; ?>
						</header><!-- .page-header -->
					<?php endif; ?>

					<div id="activity-search-form">
						<activity-search-form></activity-search-form>
					</div>
			
				</div> <!-- .row -->

			</div><!-- Container end -->

		</div>

	</div>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
