var path = require('path')
var webpack = require('webpack')
// var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

if (process.env.NODE_ENV === 'production') {
  var minimize = true;
} else {
  var minimize = false;
}

var config = {
  // TODO: Add common Configuration
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.scss$/,
        use: [
          // 'vue-style-loader',
          {
            loader: 'file-loader',
            options: {
              name: minimize ? "css/beplustheme-[name].min.css" : "css/beplustheme-[name].css",
            },
          },
          'extract-loader',
          {
            loader: 'css-loader',
            options: {
              minimize: minimize,
            }
          },
          'sass-loader',

        ],

      },
      {
        test: /\.sass$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: minimize ? "css/beplustheme-[name].min.css" : "css/beplustheme-[name].css",
            }
          },
          'extract-loader',
          {
            loader: 'css-loader',
            options: {
              minimize: minimize,
            }
          },
          'sass-loader',
          'sass-loader?indentedSyntax'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'scss': [
              'vue-style-loader',
              'css-loader',
              'sass-loader'
            ],
            'sass': [
              'vue-style-loader',
              'css-loader',
              'sass-loader?indentedSyntax'
            ]
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: minimize ? false : true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: minimize ? false : '#eval-source-map',
  plugins: [
    // new BundleAnalyzerPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    /* new webpack.optimize.UglifyJsPlugin({
      sourceMap: minimize ? false : true,
      compress: {
        warnings: false
      }
    }), */
    new webpack.LoaderOptionsPlugin({
      minimize: minimize
    })

  ]
};

var publicConfig = Object.assign({}, config, {
  name: "public",
  entry: ["./src/js/public.js", "./src/sass/public/public.scss"],
  output: {
    path: path.resolve(__dirname, './public/assets'),
    publicPath: '/public/assets/',
    filename: "js/beplustheme-public.js"
  },
});
var adminConfig = Object.assign({}, config,{
  name: "admin",
  entry: ["./src/js/admin.js", "./src/sass/admin/admin.scss"],
  output: {
      path: path.resolve(__dirname, './admin/assets'),
      publicPath: '/admin/assets/',
      filename: "js/beplustheme-admin.js"
  },
});

module.exports = [
  publicConfig, adminConfig,
];