<?php
/**
 * Understrap functions and definitions
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$beplustheme_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.

	'/template-loader.php',					// Template loader
);

foreach ( $beplustheme_includes as $file ) {
	$filepath = locate_template( '/inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

function beplustheme_menu_item() {
	add_menu_page(
		'be+THEME options', 'be+THEME', 'manage_options', 'beplustheme-options', 'beplustheme_options', 'dashicons-admin-customizer', 2
	);
}
add_action( 'admin_menu', 'beplustheme_menu_item' );

function beplustheme_load_admin_scripts() {
	
	if( $_GET['page'] == 'beplustheme-options' ) {
		if ( ! did_action( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		}
		if ( ! did_action( 'wp-color-picker' ) ) {
			wp_enqueue_style( 'wp-color-picker' );
		}
		
		wp_enqueue_style( 'beplustheme-admin', get_template_directory_uri() . '/admin/assets/css/beplustheme-admin.css' );

		wp_enqueue_script( 'color-picker', get_template_directory_uri() . '/js/color-picker.js', array( 'wp-color-picker' ), false, true );
		wp_enqueue_script( 'media-library-uploader', get_template_directory_uri() . '/js/media-library-uploader.js', array( 'jquery' ), null, false );

		wp_enqueue_style( 'bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css' );
		wp_enqueue_script( 'boot1', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
		wp_enqueue_script( 'boot2', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
		wp_enqueue_script( 'boot3', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
	}

	if( get_option( 'beplustheme_activate_aos_library' ) ) {
        wp_enqueue_style( 'aos-styles', 'https://unpkg.com/aos@2.3.1/dist/aos.css', array(), '2.3.1' );
        wp_enqueue_script( 'aos-scripts', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array(), '2.3.1');
    }
	 
}
add_action( 'admin_enqueue_scripts', 'beplustheme_load_admin_scripts' );

function beplustheme_make_post_parent_queryable() {
	if ( is_admin() )
		$GLOBALS['wp']->add_query_var( 'post_parent' );
}
add_action( 'init', 'beplustheme_make_post_parent_queryable' );