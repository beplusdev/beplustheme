<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'beplustheme_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-site-footer">

	<div id="site-footer">

		<div id="site-footer-container">

			<div class="<?php echo esc_attr( $container ); ?>">

				<div class="row">

					<div class="col-md-12">

						<footer class="site-footer" id="colophon">

							<div class="site-info">

								<?php beplustheme_site_info(); ?>

							</div><!-- .site-info -->

						</footer><!-- #colophon -->

					</div><!--col end -->

				</div><!-- row end -->

			</div><!-- container end -->

		</div>

	</div>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

