<?php
/**
 * Understrap enqueue scripts
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'beplustheme_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function beplustheme_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );
		
		$css_version = $theme_version . '.' . filemtime(get_template_directory() . '/css/theme.min.css');
		wp_enqueue_style( 'beplustheme-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $css_version );
		wp_enqueue_style( 'beplustheme-public', get_stylesheet_directory_uri() . '/public/assets/css/beplustheme-public.css', array(), false );

		$headingsName = get_option( 'beplustheme_headings_font_name' );
		$headingsWeights = get_option( 'beplustheme_headings_font_weights' );
		$bodyName = get_option( 'beplustheme_body_font_name' );
		$bodyWeights = get_option( 'beplustheme_body_font_weights' );
		if( $headingsName && $headingsWeights && $bodyName && $bodyWeights ) {
			$headingsString = $headingsName.':'.$headingsWeights;
			$bodyString = $bodyName.':'.$bodyWeights;
			wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family='.$headingsString.'|'.$bodyString, array() );
		} elseif( $headingsName && $headingsWeights ) {
			$headingsString = $headingsName.':'.$headingsWeights;
			wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family='.$headingsString, array() );
		} elseif( $bodyName && $bodyWeights ) {
			$bodyString = $bodyName.':'.$bodyWeights;
			wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family='.$bodyString, array() );
		}
		$fontsCss = beplustheme_options_load_google_fonts();
		$colorCss = beplustheme_options_set_theme_colors();
		$sizesCss = beplustheme_options_set_theme_sizes();
		wp_add_inline_style( 'beplustheme-public', $fontsCss );
		wp_add_inline_style( 'beplustheme-public', $colorCss );
		wp_add_inline_style( 'beplustheme-public', $sizesCss );

		wp_enqueue_script( 'jquery');
		
		$js_version = $theme_version . '.' . filemtime(get_template_directory() . '/js/theme.min.js');
		wp_enqueue_script( 'beplustheme-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
		wp_enqueue_script( 'beplustheme-public', get_stylesheet_directory_uri() . '/public/assets/js/beplustheme-public.js', array(), false );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'beplustheme_scripts' ).

add_action( 'wp_enqueue_scripts', 'beplustheme_scripts' );