<?php
/**
 * Template loader for be+THEME
 *
 * @package beplustheme
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'bpsDefineDisplayArray' ) ) {

    function bpsDefineDisplayArray() {

        $allDisplays = [
            'cta-sport-icon',
            'cta-activities',
            'cta-all-sport',
            'cta-reservation',
        
            'grid-square-bottom-overlay-text',
            'grid-square-bottom-text',
            'grid-square-gradient-bg',
            'grid-cpt-icons',
            'grid-cpt-activities-menu',
            'grid-image-top-info-bottom',
            
            'card-overlay-text',
            'card-contact',
            'card-activity',
        ];
        return $allDisplays;

    }

}

if ( ! function_exists( 'loadTemplate' ) ) {

    function loadTemplate( $template, $variables = array(), $single_template = false ) {

        if( is_array( $variables ) ) {
            foreach( $variables as $name => $value ) {
                set_query_var( $name, $value );
            }
        }
        if( $single_template ) {
            if( file_exists( get_stylesheet_directory() . '/' . $template ) ) {
                return load_template( getTemplatePath( $template, $variables, $single_template ) );
            } elseif( file_exists( get_stylesheet_directory() . '/public/templates/' . $template ) ) {
                return load_template( getTemplatePath( $template, $variables, $single_template ) );
            } elseif( file_exists( get_template_directory() . '/' . $template ) ) {
                return load_template( getTemplatePath( $template, $variables, $single_template ) );
            } elseif( file_exists( get_template_directory() . '/public/templates/' . $template ) ) {
                return load_template( getTemplatePath( $template, $variables, $single_template ) );
            } else {
                return load_template( getTemplatePath( $template, $variables, $single_template ) );
            }
        } else {
            if( file_exists( get_stylesheet_directory() . '/' . $template ) ) {
                return load_template( getTemplatePath( $template, $variables, $single_template ), false );
            } elseif( file_exists( get_template_directory() . '/' . $template ) ) {
                return load_template( getTemplatePath( $template, $variables, $single_template ), false );
            } else {
                return load_template( getTemplatePath( $template, $variables, $single_template ), false );
            }
        }
    
    }

}

if ( ! function_exists( 'getTemplatePath' ) ) {

    function getTemplatePath( $template, $variables = array(), $single_template = false ) {

        if( is_array( $variables ) ) {
            foreach( $variables as $name => $value ) {
                set_query_var( $name, $value );
            }
        }
        if( $single_template ) {
            if( file_exists( get_stylesheet_directory() . '/' . $template ) ) {
                return get_stylesheet_directory() . '/' . $template;
            } elseif( file_exists( get_stylesheet_directory() . '/public/templates/' . $template ) ) {
                return get_stylesheet_directory() . '/public/templates/' . $template;
            } elseif( file_exists( get_template_directory() . '/' . $template ) ) {
                return get_template_directory() . '/' . $template;
            } elseif( file_exists( get_template_directory() . '/public/templates/' . $template ) ) {
                return get_template_directory() . '/public/templates/' . $template;
            }
        } else {
            if( file_exists( get_template_directory() . '/' . $template ) ) {
                return get_template_directory() . '/' . $template;
            } elseif( file_exists( get_template_directory() . '/' . $template ) ) {
                return get_template_directory() . '/' . $template;
            } elseif( file_exists( get_stylesheet_directory() . '/' . $template ) ) {
                return get_stylesheet_directory() . '/' . $template;
            }
        }
    
    }

}

if ( ! function_exists( 'loadCptTemplate' ) ) {

    function loadCptTemplate( $atts ) {

        if( isset( $atts ) ) {
            
            $variables = shortcode_atts( array(
                /* Query related */
                'post_type' => 'news',
                'posts_per_page' => -1,
                'offset' => '0',
                'orderby' => 'date',
                'order' => 'desc',
                'post_taxonomy' => '',
                /* Meta query related */
                'meta_key' => '',
                'meta_value' => '',
                /* Post related */
                'posts_in_ids' => '',
                'posts_not_in_ids' => '',
                /* Display related */
                'display' => 'grid-square-bottom-text',
                'classes' => 'col-lg-3 col-md-2 col-12',
                /* CTA related */
                'first_cta' => false,
                'last_cta' => false,
                'cta_slug' => '',
                'cta_post_id' => '',
                'cta_description' => '',
                'cta_popup_class' => '',
                'post_parent' => false,
            ), $atts );

            // Add multiple meta_key/tax_key (mainly to be used on card_activity view)
            foreach( $atts as $att => $val ) {
                if( startsWith( $att, 'meta_key_' ) ) {
                    if( $val ) {
                        $variables['meta_queries'][$att] = $val;
                    }
                } elseif( startsWith( $att, 'tax_key_' ) ) {
                    if( $val ) {
                        $variables['tax_queries'][$att] = $val;
                    }
                }
            }
            
            $args = array(
                'post_type' => $variables['post_type'],
                'posts_per_page' => $variables['posts_per_page'],
                'offset' => $variables['offset'],
                'orderby' => $variables['orderby'],
                'order' => $variables['order'],
            );

            if ( $variables['post_parent'] !== false ) {
                $args['post_parent'] = $variables['post_parent'];
            }

            if( $variables['meta_key'] && $variables['meta_value'] ) {
                $metaQuery = array(
                    array(
                        'key' => $variables['meta_key'],
                        'value' => $variables['meta_value'],
                        'compare' => 'LIKE',
                    ),
                );
                if( !empty( $metaQuery ) ) {
                    $args['meta_query'] = $metaQuery;
                }
            } else {
                /* Add meta keys */
                if( !empty( $variables['meta_queries'] ) ) {
                    $metaQuery = array(
                        'relation' => 'AND',
                    );
                    foreach( $variables['meta_queries'] as $metaKey => $metaValue ) {
                        $metaKey = str_replace( 'meta_key_', '', $metaKey );
                        $metaElements = array(
                            'key' => $metaKey,
                            'value' => $metaValue,
                            'compare' => 'LIKE',
                        );
                        array_push( $metaQuery, $metaElements );
                    }
                    $args['meta_query'] = $metaQuery;
                }
                /* Add tax keys */
                if( !empty( $variables['tax_queries'] ) ) {
                    $taxQuery = array(
                        'relation' => 'AND',
                    );
                    foreach( $variables['tax_queries'] as $taxKey => $taxValue ) {
                        $taxKey = str_replace( 'tax_key_', '', $taxKey );
                        $taxElements = array(
                            'taxonomy' => $taxKey,
                            'field' => 'term_id',
                            'terms' => array( $metaValue ),
                        );
                        array_push( $taxQuery, $taxElements );
                    }
                    $args['tax_query'] = $taxQuery;
                }
            }
    
            $postsOptions = [
                'post_type' => $variables['post_type'],
                'posts_in_ids' => $variables['posts_in_ids'],
                'posts_not_in_ids' => $variables['posts_not_in_ids'],
                'post_taxonomy' => $variables['post_taxonomy'],
                /* 'linkedPost' => CURRENT POST ID | VALOR SELECCIONADO */
                /* 'linkedPostType => CURRENT POST TYPE | VALOR SELECCIONADO (Sobre quien lo quiero mostrar) */
                /* Saber cuándo se utiliza en CPT o en Page */
            ];
    
            $displayOptions = [
                'display' => $variables['display'],
                'classes' => $variables['classes'],
            ];

            $ctaOptions = [
                'first_cta' => $variables['first_cta'],
                'last_cta' => $variables['last_cta'],
                'cta_slug' => $variables['cta_slug'],
                'cta_post_id' => $variables['cta_post_id'],
                'cta_description' => $variables['cta_description'],
                'cta_popup_class' => $variables['cta_popup_class'],
            ];

            if( $postsIn = $postsOptions['posts_in_ids'] ) {
                $args['post__in'] = explode( ',', $postsIn );
            } elseif( $postsNotIn = $postsOptions['posts_not_in_ids'] ) {
                $args['post__not_in'] = explode( ',', $postsNotIn );
            }
    
            $posts = get_posts( $args );
    
            ?>
            
            <?php ob_start(); ?>            
            
            <?php loadTemplate( 
                'public/templates/loadCptTemplate.php',
                [
                    'queriedPosts' => $posts,
                    'postsOptions' => $postsOptions,
                    'displayOptions' => $displayOptions,
                    'ctaOptions' => $ctaOptions,
                    ]
                ); ?>
            
            <?php wp_reset_postdata(); ?>
            <?php return ob_get_clean(); ?>
    
            <?php
    
        } else {
    
            _e( 'No parameters found in shortcode', 'beplusplugin' );
    
        }
        
    }
    add_shortcode( 'load-cpt-template', 'loadCptTemplate' );

}