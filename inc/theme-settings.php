<?php
/**
 * Check and setup theme's default settings
 *
 * @package beplustheme
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/* be+THEME options */
if ( ! function_exists( 'beplustheme_options' ) ) {
	function beplustheme_options() {
		if( !current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.', 'beplustheme' ) );
		}
		?>

		<div class="wrap">

			<?php /* print_r($_POST); */ ?>
			<?php beplustheme_save_theme_options(); ?>

			<h1 style="font-size: 2rem;"><?php _e( 'be+THEME options', 'beplustheme' ); ?></h1>
			<p><?php _e( 'Here we can set up the options for this theme', 'beplustheme' ); ?></p>					

			<form method="post" action="" class="d-flex">

				<?php wp_nonce_field( 'beplustheme_settings_update' ); ?>

				<?php $arrayTabs = [
					['bpt-theme-images', 'Theme images'],
					['bpt-styling-options', 'Styling'],
					['bpt-google-fonts', 'Google Fonts'],
					['bpt-cpt-fields', 'CPT Fields'],
					['bpt-page-sizes', 'Page sizes'],
					['bpt-page-colors', 'Page colors'],
					['bpt-activity-form-options', 'Activity Form'],
					['bpt-footer-options', 'Footer'],
					['bpt-social-networks', 'Social Networks'],
					['bpt-api-keys', 'API Keys'],
					['bpt-custom-css', 'Custom CSS']
				]; ?>

				<?php beplustheme_options_tab_menu( $arrayTabs ); ?>

				<div id="beplustheme-options-tab-menu-v-pills-tabContent" class="tab-content beplustheme-options-form col-9">

					<?php foreach( $arrayTabs as $tab ) : ?>

						<div class="tab-pane fade <?php echo $arrayTabs[0] == $tab ? 'show active' : ''; ?>" id="<?php echo $tab[0]; ?>" role="tabpanel" aria-labelledby="<?php echo 'v-pills-'.$tab[0].'-tab'; ?>">

							<?php beplustheme_options_get_option_set( $tab[0] ); ?>

						</div>

					<?php endforeach; ?>

				</div>

			</form>

		</div>

		<?php

	}
}

if ( ! function_exists( 'beplustheme_options_tab_menu' ) ) {

	function beplustheme_options_tab_menu( $arrayTabs ) {

		?>
		<ul id="beplustheme-options-tab-menu-v-pills-tab" class="nav flex-column nav-pills col-3" role="tablist" aria-orientation="vertical">
			<?php foreach( $arrayTabs as $tab ) : ?>
				<li class="nav-item">
					<a class="<?php echo $tab == $arrayTabs[0] ? 'active' : ''; ?> nav-link beplustheme-tab-menu-item" data-toggle="pill" href="#<?php echo $tab[0]; ?>" role="tab" aria-controls="<?php echo $tab[0]; ?>" aria-selected="<?php echo $tab == $arrayTabs[0] ? 'true' : 'false'; ?>"><?php echo $tab[1]; ?></a>
				</li>
			<?php endforeach; ?>
		</ul>

		<?php

	}

}

if ( ! function_exists( 'beplustheme_options_get_option_set' ) ) {

	function beplustheme_options_get_option_set( $option ) {

		switch( $option ) {
			case 'bpt-theme-images' :
				?>
				<h4><?php _e( 'be+THEME images', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Website Logo (max height: 70px)', 'beplustheme' ); ?></p>
						<?php beplustheme_image_uploader( 'beplustheme_website_logo', 150, 150 ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Favicon (16x16px)', 'beplustheme' ); ?></p>
						<?php beplustheme_image_uploader( 'beplustheme_website_favicon', 150, 150 ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Header background image', 'beplustheme' ); ?></p>
						<?php beplustheme_image_uploader( 'beplustheme_header_image', 150, 150 ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Footer background image', 'beplustheme' ); ?></p>
						<?php beplustheme_image_uploader( 'beplustheme_footer_image', 150, 150 ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Default icon', 'beplustheme' ); ?></p>
						<?php beplustheme_image_uploader( 'beplustheme_default_icon', 150, 150 ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Default logo', 'beplustheme' ); ?></p>
						<?php beplustheme_image_uploader( 'beplustheme_default_logo', 150, 150 ); ?>
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-styling-options' :
				?>
				<h4><?php _e( 'Styling options', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Activate JS transitions? (AOS JS library)', 'beplustheme' ); ?></p>
						<input type="checkbox" name="beplustheme_activate_aos_library" value="1" <?php echo get_option( 'beplustheme_activate_aos_library' ) ? 'checked' : ''; ?>>
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-google-fonts' :
				?>
				<h4><?php _e( 'Google Fonts', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Headings font name', 'beplustheme' ); ?></p>
						<input style="width: 100%;" type="text" name="beplustheme_headings_font_name" placeholder="<?php _e( 'Example: Roboto+Condensed', 'beplustheme' ); ?>" value="<?php echo get_option( 'beplustheme_headings_font_name' ); ?>">
					</div>

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Headings font weights', 'beplustheme' ); ?></p>
						<input style="width: 100%;" type="text" name="beplustheme_headings_font_weights" placeholder="<?php _e( 'Example: 400,400i,700', 'beplustheme' ); ?>" value="<?php echo get_option( 'beplustheme_headings_font_weights' ); ?>">
					</div>

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Headings font CSS', 'beplustheme' ); ?></p>
						<input style="width: 100%;" type="text" name="beplustheme_headings_font_css" placeholder="<?php _e( 'Example: \'Roboto Condensed\', sans-serif', 'beplustheme' ); ?>" value="<?php echo stripslashes( get_option( 'beplustheme_headings_font_css' ) ); ?>">
					</div>

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Body font name', 'beplustheme' ); ?></p>
						<input style="width: 100%;" type="text" name="beplustheme_body_font_name" placeholder="<?php _e( 'Example: Roboto+Condensed', 'beplustheme' ); ?>" value="<?php echo get_option( 'beplustheme_body_font_name' ); ?>">
					</div>

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Body font weights', 'beplustheme' ); ?></p>
						<input style="width: 100%;" type="text" name="beplustheme_body_font_weights" placeholder="<?php _e( 'Example: 400,400i,700', 'beplustheme' ); ?>" value="<?php echo get_option( 'beplustheme_body_font_weights' ); ?>">
					</div>

					<div class="col-lg-4 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Body font CSS', 'beplustheme' ); ?></p>
						<input style="width: 100%;" type="text" name="beplustheme_body_font_css" placeholder="<?php _e( 'Example: \'Roboto Condensed\', sans-serif', 'beplustheme' ); ?>" value="<?php echo stripslashes( get_option( 'beplustheme_body_font_css' ) ); ?>">
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-cpt-fields' :
				?>
				<h4><?php _e( 'CPT default fields' ); ?></h4>

				<div class="row mt-3 mb-3">

					<?php if( is_plugin_active( 'beplusplugin/beplusplugin.php' ) ) : ?>
						<?php $bppHelper = new BPS\BePlusPlugin\Helpers\BePlusPluginHelpers(); ?>
						<?php $allActiveModules = $bppHelper->getActiveCptModules(); ?>
						<?php foreach( $allActiveModules as $module ) : ?>
							<?php $moduleSlug = $bppHelper->removeModuleSuffix( $module ); ?>
														
								<div class="col-lg-12">
									<h5><?php echo sprintf( __( '%s module', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></h5>
									<div class="row">
										<div class="col-lg-3 col-md-4 col-12 bps-option-column">
											<p class="bps-option-title"><?php echo sprintf( __( '%s archive title', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></p>
											<input type="text" name="<?php echo $moduleSlug.'_archive_title'; ?>" value="<?php echo stripslashes( get_option( $moduleSlug.'_archive_title' ) ); ?>">
										</div>
										<div class="col-lg-3 col-md-4 col-12 bps-option-column">
											<p class="bps-option-title"><?php echo sprintf( __( '%s archive description', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></p>
											<textarea rows="4" cols="50" name="<?php echo $moduleSlug.'_archive_description'; ?>"><?php echo stripslashes( get_option( $moduleSlug.'_archive_description' ) ); ?></textarea>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-3 col-md-3 col-12 bps-option-column">
											<p class="bps-option-title"><?php echo sprintf( __( '%s archive background image', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></p>
											<?php beplustheme_image_uploader( $moduleSlug.'_archive_background_image', 150, 150 ); ?>
										</div>
										<div class="col-lg-3 col-md-3 col-12 bps-option-column">
											<p class="bps-option-title"><?php echo sprintf( __( '%s single background image', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></p>
											<?php beplustheme_image_uploader( $moduleSlug.'_single_background_image', 150, 150 ); ?>
										</div>
										<div class="col-lg-3 col-md-3 col-12 bps-option-column">
											<p class="bps-option-title"><?php echo sprintf( __( '%s default logo', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></p>
											<?php beplustheme_image_uploader( $moduleSlug.'_default_logo', 150, 150 ); ?>
										</div>
										<div class="col-lg-3 col-md-3 col-12 bps-option-column">
											<p class="bps-option-title"><?php echo sprintf( __( '%s default image', 'beplustheme' ), ucfirst( $moduleSlug ) ); ?></p>
											<?php beplustheme_image_uploader( $moduleSlug.'_default_icon', 150, 150 ); ?>
										</div>
									</div>
								</div>

						<?php endforeach; ?>
					<?php else : ?>
						<?php _e( 'No CPT modules active', 'beplustheme' ); ?>
					<?php endif; ?>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-page-sizes' :
				?>
				<h4><?php _e( 'Page sizes', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Page width extra large ( >= 1200px )', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_page_width_extra_large" value="<?php echo get_option( 'beplustheme_page_width_extra_large' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Page width large ( >= 992px )', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_page_width_large" value="<?php echo get_option( 'beplustheme_page_width_large' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Page width medium ( >= 768px )', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_page_width_medium" value="<?php echo get_option( 'beplustheme_page_width_medium' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Page width small ( >= 576px )', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_page_width_small" value="<?php echo get_option( 'beplustheme_page_width_small' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Page width extra small ( < 576px )', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_page_width_extra_small" value="<?php echo get_option( 'beplustheme_page_width_extra_small' ); ?>">
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-page-colors' :
				?>
				<h4><?php _e( 'Page colors', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Main color', 'beplustheme' ); ?></p>
						<?php beplustheme_color_picker_options( 'beplustheme_main_color' ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Secondary color', 'beplustheme' ); ?></p>
						<?php beplustheme_color_picker_options( 'beplustheme_secondary_color' ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Text color', 'beplustheme' ); ?></p>
						<?php beplustheme_color_picker_options( 'beplustheme_text_color' ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Headings color', 'beplustheme' ); ?></p>
						<?php beplustheme_color_picker_options( 'beplustheme_headings_color' ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Link color', 'beplustheme' ); ?></p>
						<?php beplustheme_color_picker_options( 'beplustheme_link_color' ); ?>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Link hover/active color', 'beplustheme' ); ?></p>
						<?php beplustheme_color_picker_options( 'beplustheme_link_hover_active_color' ); ?>
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-activity-form-options' :
				?>
				<h4><?php _e( 'Activity form options', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Morning begin hour (00-23)', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_morning_hour" value="<?php echo get_option( 'beplustheme_morning_hour' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Afternoon begin hour (00-23)', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_afternoon_hour" value="<?php echo get_option( 'beplustheme_afternoon_hour' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Evening begin hour (00-23)', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_evening_hour" value="<?php echo get_option( 'beplustheme_evening_hour' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Finish hour (00-23)', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_finish_hour" value="<?php echo get_option( 'beplustheme_finish_hour' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Display sport?', 'beplustheme' ); ?></p>
						<input type="checkbox" name="beplustheme_display_sport" value="1" <?php echo get_option( 'beplustheme_display_sport' ) ? 'checked' : ''; ?>>
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Display club?', 'beplustheme' ); ?></p>
						<input type="checkbox" name="beplustheme_display_club" value="1" <?php echo get_option( 'beplustheme_display_club' ) ? 'checked' : ''; ?>>
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-footer-options' :
				?>
				<h4><?php _e( 'Footer options', 'beplustheme' ); ?></h4>

				<div class="d-flex-column mt-3 mb-3">

					<div class="row">

						<div class="col-lg-9 col-md-9 col-9 px-3 bps-option-column">
							<p class="bps-option-title"><?php _e( 'Footer box 1', 'beplustheme' ); ?></p>
							<textarea rows="4" cols="80" name="beplustheme_footer_box_1"><?php echo stripslashes( get_option( 'beplustheme_footer_box_1' ) ); ?></textarea>
						</div>

						<div class="col-lg-3 col-md-3 col-3 px-3 bps-option-column">
							<p class="bps-option-title"><?php _e( 'Contact info in box 1', 'beplustheme' ); ?></p>
							<input type="checkbox" name="beplustheme_display_contact_info_box_1" value="1" <?php echo get_option( 'beplustheme_display_contact_info_box_1' ) ? 'checked' : ''; ?>>
						</div>

					</div>

					<div class="row">

						<div class="col-lg-9 col-md-9 col-9 px-3 bps-option-column">
							<p class="bps-option-title"><?php _e( 'Footer box 2', 'beplustheme' ); ?></p>
							<textarea rows="4" cols="80" name="beplustheme_footer_box_2"><?php echo stripslashes( get_option( 'beplustheme_footer_box_2' ) ); ?></textarea>
						</div>

						<div class="col-lg-3 col-md-3 col-3 px-3 bps-option-column">
							<p class="bps-option-title"><?php _e( 'Contact info in box 2', 'beplustheme' ); ?></p>
							<input type="checkbox" name="beplustheme_display_contact_info_box_2" value="1" <?php echo get_option( 'beplustheme_display_contact_info_box_2' ) ? 'checked' : ''; ?>>
						</div>
					
					</div>

					<div class="row">

						<div class="col-lg-9 col-md-9 col-9 px-3 bps-option-column">
							<p class="bps-option-title"><?php _e( 'Footer box 3', 'beplustheme' ); ?></p>
							<textarea rows="4" cols="80" name="beplustheme_footer_box_3"><?php echo stripslashes( get_option( 'beplustheme_footer_box_3' ) ); ?></textarea>
						</div>

						<div class="col-lg-3 col-md-3 col-3 px-3 bps-option-column">
							<p class="bps-option-title"><?php _e( 'Contact info in box 3', 'beplustheme' ); ?></p>
							<input type="checkbox" name="beplustheme_display_contact_info_box_3" value="1" <?php echo get_option( 'beplustheme_display_contact_info_box_3' ) ? 'checked' : ''; ?>>
						</div>

					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-social-networks' :
				?>
				<h4><?php _e( 'Social networks', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Contact phone', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_contact_phone" value="<?php echo get_option( 'beplustheme_contact_phone' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Contact email', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_contact_email" value="<?php echo get_option( 'beplustheme_contact_email' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Facebook link', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_facebook_link" value="<?php echo get_option( 'beplustheme_facebook_link' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Twitter link', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_twitter_link" value="<?php echo get_option( 'beplustheme_twitter_link' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'LinkedIn link', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_linkedin_link" value="<?php echo get_option( 'beplustheme_linkedin_link' ); ?>">
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-api-keys' :
				?>
				<h4><?php _e( 'API Keys', 'beplustheme' ); ?></h4>

				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Google Maps API Key', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_gmaps_api_key" value="<?php echo get_option( 'beplustheme_gmaps_api_key' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Google reCaptcha Public Key', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_grecaptcha_public_key" value="<?php echo get_option( 'beplustheme_grecaptcha_public_key' ); ?>">
					</div>

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<p class="bps-option-title"><?php _e( 'Google reCaptcha Private Key', 'beplustheme' ); ?></p>
						<input type="text" name="beplustheme_grecaptcha_private_key" value="<?php echo get_option( 'beplustheme_grecaptcha_private_key' ); ?>">
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			case 'bpt-custom-css' :
				?>
				<h4><?php _e( 'Custom CSS', 'beplustheme' ); ?></h4>
						
				<div class="row mt-3 mb-3">

					<div class="col-lg-3 col-md-4 col-12 bps-option-column">
						<textarea rows="14" cols="110" name="beplustheme_custom_css"><?php echo get_option( 'beplustheme_custom_css' ); ?></textarea>
					</div>

				</div>

				<?php submit_button(); ?>
				<?php
				break;
			default :
				_e( 'Please, add the tab name to the array in "beplustheme_options" and the case in "beplustheme_options_get_option_set" functions', 'beplustheme' );
				break;
		}

	}

}

if ( ! function_exists( 'beplustheme_options_exists_website_logo' ) ) {
	
	function beplustheme_options_exists_website_logo() {

		$optionsLogo = get_option( 'beplustheme_website_logo' );
		if( !$optionsLogo ) {
			return false;
		} else {
			return true;
		}

	}

}

if ( ! function_exists ( 'beplustheme_options_get_website_logo' ) ) {

	function beplustheme_options_get_website_logo() {

		$optionsLogo = get_option( 'beplustheme_website_logo' );
		$imageHtml = wp_get_attachment_image( $optionsLogo, 'full', false );
		echo '<a href="' . esc_url( home_url( '/' ) ) . '" class="custom-logo-link">' . $imageHtml . '</a>';

	}

}

if ( ! function_exists( 'beplustheme_options_set_theme_sizes' ) ) {
	function beplustheme_options_set_theme_sizes() {
		
		ob_start();

		$pageWidthExtraLarge = get_option( 'beplustheme_page_width_extra_large' );
		$pageWidthLarge = get_option( 'beplustheme_page_width_large' );
		$pageWidthMedium = get_option( 'beplustheme_page_width_medium' );
		$pageWidthSmall = get_option( 'beplustheme_page_width_small' );
		$pageWidthExtraSmall = get_option( 'beplustheme_page_width_extra_small' );

		if( !empty( $pageWidthExtraLarge ) ) {
			?>
			@media ( min-width: 1200px ) {
				.container {
					max-width: <?php echo $pageWidthExtraLarge; ?>px;
				}
			}
			<?php
		}
		
		if (!empty( $pageWidthLarge ) ) {
			?>
			@media ( min-width: 992px ) {
				.container {
					max-width: <?php echo $pageWidthLarge; ?>px;
				}
			}
			<?php
		}
		
		if (!empty( $pageWidthMedium ) ) {
			?>
			@media ( min-width: 768px ) {
				.container {
					max-width: <?php echo $pageWidthMedium; ?>px;
				}
			}
			<?php
		}
		
		if (!empty( $pageWidthSmall ) ) {
			?>
			@media ( min-width: 576px ) {
				.container {
					max-width: <?php echo $pageWidthSmall; ?>px;
				}
			}
			<?php
		}

		if (!empty( $pageWidthExtraSmall ) ) {
			?>
			.container {
				max-width: <?php echo $pageWidthExtraSmall; ?>px;
			}
			<?php
		}

		$css = ob_get_clean();
		return $css;
	}
}

if ( ! function_exists ( 'beplustheme_options_set_theme_colors' ) ) {

	function beplustheme_options_set_theme_colors() {

		ob_start();

		$mainColor = get_option( 'beplustheme_main_color' );
		$secondaryColor = get_option( 'beplustheme_secondary_color' );
		$textColor = get_option( 'beplustheme_text_color' );
		$headerColor = get_option( 'beplustheme_headings_color' );
		$linkColor = get_option( 'beplustheme_link_color' );
		$linkHoverActiveColor = get_option( 'beplustheme_link_hover_active_color' );
		
		if( !empty( $mainColor ) ) {
			?>
			.bg-primary, .btn-primary {
				background-color: <?php echo $mainColor; ?> !important;
				border-color: <?php echo $mainColor; ?> !important;
			}
			.primary {
				color: <?php echo $mainColor; ?> !important;
			}
			<?php
		}

		if( !empty( $secondaryColor ) ) {
			?>
			.bg-secondary, .btn-secondary {
				background-color: <?php echo $secondaryColor; ?> !important;
			}
			.secondary {
				color: <?php echo $secondaryColor; ?> !important;
			}
			<?php
		}

		if( !empty( $textColor ) ) {
			?>
			body {
				color: <?php echo $textColor; ?>;
			}
			<?php
		}

		if( !empty( $headerColor ) ) {
			?>
			h1, h2, h3, h4, h5. h6 {
				color: <?php echo $headerColor; ?>;
			}
			<?php
		}

		if( !empty( $linkColor ) ) {
			?>
			a {
				color: <?php echo $linkColor; ?>;
			}
			<?php
		}

		if( !empty( $linkHoverActiveColor ) ) {
			?>
			a:hover, a:active {
				color: <?php echo $linkHoverActiveColor; ?>;
			}
			<?php
		}

		$css = ob_get_clean();
		return $css;

	}

}

if ( ! function_exists( 'beplustheme_options_load_google_fonts' ) ) {

	function beplustheme_options_load_google_fonts() {

		$headingsName = get_option( 'beplustheme_headings_font_name' );
		$headingsWeights = get_option( 'beplustheme_headings_font_weights' );
		$headingsCss = get_option( 'beplustheme_headings_font_css' );
		$bodyName = get_option( 'beplustheme_body_font_name' );
		$bodyWeights = get_option( 'beplustheme_body_font_weights' );
		$bodyCss = get_option( 'beplustheme_body_font_css' );

		ob_start();
		
		if( $headingsCss && $headingsName && $headingsWeights ) {
			?>
			h1, h2, h3, h4, h5, h6 {
				font-family: <?php echo stripslashes( $headingsCss ); ?>;
			}
			<?php
		}

		if( $bodyCss && $bodyName && $bodyWeights ) {
			?>
			body {
				font-family: <?php echo stripslashes( $bodyCss ); ?>;
			}
			<?php
		}

		$css = ob_get_clean();
		return $css;

	}

}

if ( ! function_exists( 'beplustheme_image_uploader' ) ) {
	function beplustheme_image_uploader( $name, $width, $height ) {

		// Set variables
		$options = get_option( $name );

		if ( $options ) {
			$imageAttributes = wp_get_attachment_image_src( $options, array( $width, $height ) );
			$src = $imageAttributes[0];
			$value = $options;
		} else {
			$src = '';
			$value = '';
		}

		$text = __( 'Upload', 'beplustheme' );

		// Print HTML field
		echo '
			<div class="upload">
				<img data-src="' . $src . '" src="' . $src . '" width="' . $width . 'px" height="' . $height . 'px" style="object-fit: contain;" />
				<div>
					<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
					<button type="submit" class="upload_image_button button">' . $text . '</button>
					<button type="submit" class="remove_image_button button">&times;</button>
				</div>
			</div>
		';
	}
}

if( ! function_exists( 'beplustheme_color_picker_options' ) ) {
	function beplustheme_color_picker_options( $name ) {

		$value = get_option( $name );
		echo '<input type="text" name="' . $name . '" value="' . $value . '" class="beplustheme-color-picker" >';

	}
}

if ( ! function_exists( 'beplustheme_save_theme_options' ) ) {
	function beplustheme_save_theme_options() {

		$moduleFields = [];
		if( is_plugin_active( 'beplusplugin/beplusplugin.php' ) ) {
			$bppHelper = new BPS\BePlusPlugin\Helpers\BePlusPluginHelpers();
			$allActiveModules = $bppHelper->getActiveCptModules();
			foreach( $allActiveModules as $module ) {
				$moduleSlug = $bppHelper->removeModuleSuffix( $module );
				array_push( $moduleFields, $moduleSlug.'_archive_title', $moduleSlug.'_archive_description', $moduleSlug.'_archive_background_image', $moduleSlug.'_single_background_image', $moduleSlug.'_default_logo', $moduleSlug.'_default_icon' );
			}
		}

		$arrayFields = [
			'beplustheme_website_logo',
			'beplustheme_website_favicon',
			'beplustheme_header_image',
			'beplustheme_footer_image',
			'beplustheme_default_icon',
			'beplustheme_default_logo',
			'beplustheme_activate_aos_library',
			'beplustheme_headings_font_name',
			'beplustheme_headings_font_weights',
			'beplustheme_headings_font_css',
			'beplustheme_body_font_name',
			'beplustheme_body_font_weights',
			'beplustheme_body_font_css',
			'beplustheme_page_width_extra_large',
			'beplustheme_page_width_large',
			'beplustheme_page_width_medium',
			'beplustheme_page_width_small',
			'beplustheme_page_width_extra_small',
			'beplustheme_main_color',
			'beplustheme_secondary_color',
			'beplustheme_text_color',
			'beplustheme_headings_color',
			'beplustheme_link_color',
			'beplustheme_link_hover_active_color',
			'beplustheme_morning_hour',
			'beplustheme_afternoon_hour',
			'beplustheme_evening_hour',
			'beplustheme_finish_hour',
			'beplustheme_display_sport',
			'beplustheme_display_club',
			'beplustheme_footer_box_1',
			'beplustheme_footer_box_2',
			'beplustheme_footer_box_3',
			'beplustheme_display_contact_info_box_1',
			'beplustheme_display_contact_info_box_2',
			'beplustheme_display_contact_info_box_3',
			'beplustheme_contact_phone',
			'beplustheme_contact_email',
			'beplustheme_facebook_link',
			'beplustheme_twitter_link',
			'beplustheme_linkedin_link',
			'beplustheme_gmaps_api_key',
			'beplustheme_grecaptcha_public_key',
			'beplustheme_grecaptcha_private_key',
			'beplustheme_custom_css',
		];
		$arrayFields = array_merge( $arrayFields, $moduleFields );
		if ( wp_verify_nonce( $_POST['_wpnonce'], 'beplustheme_settings_update' ) ) :
			foreach( $arrayFields as $field ) :
				if( $_POST[ $field ] ) :
					update_option( $field, $_POST[ $field ] );
				else :
					update_option( $field, '' );
				endif;
			endforeach;
		endif;
	}
}

if ( ! function_exists ( 'beplustheme_setup_theme_default_settings' ) ) {
	function beplustheme_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$beplustheme_posts_index_style = get_theme_mod( 'beplustheme_posts_index_style' );
		if ( '' == $beplustheme_posts_index_style ) {
			set_theme_mod( 'beplustheme_posts_index_style', 'default' );
		}

		// Sidebar position.
		$beplustheme_sidebar_position = get_theme_mod( 'beplustheme_sidebar_position' );
		if ( '' == $beplustheme_sidebar_position ) {
			set_theme_mod( 'beplustheme_sidebar_position', 'right' );
		}

		// Container width.
		$beplustheme_container_type = get_theme_mod( 'beplustheme_container_type' );
		if ( '' == $beplustheme_container_type ) {
			set_theme_mod( 'beplustheme_container_type', 'container' );
		}
	}
}

if ( ! function_exists( 'beplustheme_load_header_image' ) ) {

	function beplustheme_load_header_image() {

		if( !is_archive() && !is_single() && !is_page() ) {
			if( has_post_thumbnail( $wp_query->post->ID ) ) :
			$post_image_id = get_post_thumbnail_id( $wp_query->post->ID );
			$post_image = wp_get_attachment_image_src( $post_image_id, 'full' );
			?>
			<div class="wrapper-post-featured-image">
				<img src="<?php echo $post_image[0]; ?>" />
			</div>
			<?php
			elseif( $header_image_id = get_option( 'beplustheme_header_image' ) ) :
				$header_image = wp_get_attachment_image_src( $header_image_id, 'full' );
				?>
				<div class="wrapper-header-image">
					<img src="<?php echo $header_image[0]; ?>" />
				</div>
				<?php
			endif;
		}

	}
}

if ( ! function_exists( 'beplustheme_save_gmaps_api_key_acf' ) ) {

	function beplustheme_save_gmaps_api_key_acf() {

		$apiKey = get_option( 'beplustheme_gmaps_api_key' );
		acf_update_setting( 'google_api_key', $apiKey );
	}
	add_action( 'acf/init', 'beplustheme_save_gmaps_api_key_acf' );

}

/* 
 * Aux functions 
 */

/* Add custom menus */
if ( ! function_exists( 'registerCustomMenus' ) ) {

	function registerCustomMenus() {

		register_nav_menu( 'footer-menu', __( 'Footer Menu', 'beplustheme' ) );
		register_nav_menu( 'subheader-menu', __( 'Sub Header Menu', 'beplustheme' ) );

	}
	add_action( 'after_setup_theme', 'registerCustomMenus' );

}

/* Change query parameters when needed */
if ( ! function_exists( 'beplusthemeCustomQueries' ) ) {

	/* function beplusthemeCustomQueries( $query ) {
	}
	add_action('pre_get_posts', 'beplusthemeCustomQueries' ); */

}

/* Display contact info in footer boxes (or wherever it's needed) */
if ( ! function_exists( 'bpsDisplayContactInfo' ) ) {

	function bpsDisplayContactInfo() {
		?>

		<div class="bps-footer-contact-data">
			<?php if( $contactPhone = get_option('beplustheme_contact_phone') ) : ?>
				<a target="_blank" href="tel:<?php echo str_replace( ".", "", str_replace( " ", "", $contactPhone ) ); ?>" class="bps-footer-contact-phone-icon"><?php echo $contactPhone; ?></a>
				<br/>
			<?php endif; ?>
			<?php if( $contactEmail = get_option('beplustheme_contact_email') ) : ?>
				<a target="_blank" href="mailto:<?php echo $contactEmail; ?>" class="bps-footer-contact-email-icon"><?php echo $contactEmail; ?></a>
			<?php endif; ?>
		</div>

		<?php
	}

}

/* Get CPT logo in template */
if ( ! function_exists( 'bpsGetCptLogoInTemplate' ) ) {
	
	function bpsGetCptLogoInTemplate( $postType, $postID ) {
		if ( $logo = get_field( $postType.'_info_logo', $postID ) ) {
			$logoUrl = $logo['url'];
		} elseif( $defaultLogo = get_option( $postType.'_default_logo' ) ) {
			$logoUrl = wp_get_attachment_image_src( $defaultLogo )[0];
		} else {
			$logoUrl = wp_get_attachment_image_src( get_option( 'beplustheme_default_logo' ) )[0];
		}
		return $logoUrl;
	}

}

/* Get CPT icon in template */
if ( ! function_exists( 'bpsGetCptIconInTemplate' ) ) {
	
	function bpsGetCptIconInTemplate( $postType, $postID ) {
		if ( $icon = get_field( $postType.'_info_icon', $postID ) ) {
			$iconUrl = $icon['url'];
		} else {
			$iconUrl = bpsGetCptIcon( $postType );
		}
		return $iconUrl;
	}

}

/* Get CPT image in template */
if ( ! function_exists( 'bpsGetCptImageInTemplate' ) ) {
	
	function bpsGetCptImageInTemplate( $postType, $postID = '' ) {
		if ( has_post_thumbnail( $postID ) ) {
			$imageUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $postID ), 'full' )[0];
		} elseif( $defaultSingleImage = get_option( $postType.'_single_background_image' ) && is_single( $postID ) ) {
			$imageUrl = wp_get_attachment_image_src( $defaultSingleImage )[0];
		} elseif( $defaultArchiveImage = get_option( $postType.'_archive_background_image' ) ) {
			$imageUrl = wp_get_attachment_image_src( $defaultArchiveImage )[0];
		}
		return $imageUrl;
	}

}

/* Get CPT taxonomies in template */
if ( ! function_exists( 'bpsGetCptTaxonomies' ) ) {

	function bpsGetCptTaxonomies( $postTaxonomies, $postID ) {
		$postCategoriesArray = get_the_terms( $postID, $postTaxonomies );
		$categoriesArray = array();
		if( !empty( $postCategoriesArray ) ) {
			foreach( $postCategoriesArray as $key => $category ) {
				$categoriesArray[] = $category->name;
			}
		}
		return $categoriesArray;
	}

}

/* Get CPT icon */
if ( ! function_exists( 'bpsGetCptIcon' ) ) {

	function bpsGetCptIcon( $postType ) {
		if( $defaultIcon = get_option( $postType.'_default_icon' ) ) {
			$iconUrl = wp_get_attachment_image_src( $defaultIcon )[0];
		} else {
			$iconUrl = wp_get_attachment_image_src( get_option( 'beplustheme_default_icon' ) )[0];
		}
		return $iconUrl;
	}

}

/* Get sport activities in the system */
if ( ! function_exists( 'getAllItemActivities' ) ) {

	function getAllItemActivities( $postType, $postID ) {

		$itemActivities = new WP_Query(
			array(
				'post_type' => 'activity',
				'posts_per_page' => -1,
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'activity_'.$postType,
						'value' => $postID,
						'compare' => '=',
					)
				)
			)
		);
		return $itemActivities;

	}

}

/* Get activity hours in card-activity template */
if ( ! function_exists( 'getActivityHoursInTemplate' ) ) {

	function getActivityHoursInTemplate( $postID ) {

		$daysGroupedBy = get_field( 'activity_group_by_day', $postID );

		if( $daysGroupedBy ) {
			$activityDays = getActivityDatesGroupedByDays( $post->ID );
		} else {
			$activityDays = getActivityDatesGroupedByHours( $post->ID );
		}

		return $activityDays;

	}

}

/* Show Activity info in single-activity */
if( ! function_exists( 'addActivityInfoToSingle' ) ) {

	function addActivityInfoToSingle( $postID ) {

		/* Get vars to make table */
		/* $ageFrom = get_field( 'activity_age_from', $postID );
		$ageTo = get_field( 'activity_age_to', $postID );
		$infrastructureObject = get_field( 'activity_infrastructure', $postID );
		$infrastructureNonObject = get_field( 'activity_location', $postID ); */
		$sportObject = get_field( 'activity_sport', $postID );
		$sportNonObject = get_field( 'activity_sport_name', $postID );
		$clubObject = get_field( 'activity_club', $postID );
		$clubNonObject = get_field( 'activity_club_name', $postID );
		$activityPrice = get_field( 'activity_price', $postID );
		$activityInfo = get_field( 'activity_info', $postID );

		$daysHours = array();

		?>

		<div class="activity-info">

		<?php
		/* Show age */
		/* if( $ageFrom || $ageTo ) :
			?>
			<div class="activity-age-range">
				<span class="activity-age-range-data">
				<?php
					if( $ageFrom && !$ageTo ) {
						printf( __( 'From %d years', 'beplustheme' ), $ageFrom );
					} elseif( !$ageFrom && $ageTo ) {
						printf( __( 'Up to %d years', 'beplustheme' ), $ageTo );
					} else {
						printf( __( 'From %d to %d years', 'beplustheme' ), $ageFrom, $ageTo );
					}
				?>
				</span>
			</div>
			<?php
		endif; */

		/* Show sport */
		if( $sportObject || $sportNonObject ) :
			?>
			<div class="activity-sport">
				<span class="activity-sport-data">
				<?php
				if( $sportObject ) {
					?>
					<a class="activity-sport-link" href="<?php echo get_permalink( $sportObject->ID ); ?>"><?php echo $sportObject->post_title; ?></a>
					<?php
				} elseif( $sportNonObject ) {
					echo $sportNonObject;
				}
				?>
				</span>
			</div>
			<?php
		endif;

		/* Show club */
		if( $clubObject || $clubNonObject ) :
			?>
			<div class="activity-club">
				<span class="activity-club-data">
				<?php
				if( $clubObject ) {
					?>
					<a class="activity-club-link" href="<?php echo get_permalink( $clubObject->ID ); ?>"><?php echo $clubObject->post_title; ?></a>
					<?php
				} elseif( $clubNonObject ) {
					echo $clubNonObject;
				}
				?>
				</span>
			</div>
			<?php
		endif;

		/* Show infrastructure (if is object) */
		/* if( $infrastructureObject ) :
			?>
			<div class="activity-infrastructure">
				<span class="activity-infrastructure-data">
				<?php
				if( $infrastructureObject ) {
					?>
					<a class="activity-infrastructure-link" href="<?php echo get_permalink( $infrastructureObject->ID ); ?>"><?php echo $infrastructureObject->post_title; ?></a>
					<?php
				}
				?>
				</span>
			</div>
			<?php
		endif; */

		?>
		<?php if( $activityPrice ) : ?>
			<div class="activity-price">
				<?php
				if( $activityPrice > 0 ) :
					?>
					<span class="activity-price-data"><?php echo $activityPrice; ?></span>
					<?php
				else :
					?>
					<span class="activity-price-data"><?php _e( 'Free', 'beplustheme' ); ?></span>
					<?php
				endif;
				?>
			</div>
		<?php endif; ?>

		</div>

		<?php

		/* Show a table with all the dates for the activity */
		buildActivityDaysTable( $postID );

		/* if( !$infrastructureObject && $infrastructureNonObject ) {
			?>
			<p><?php echo '<i class="fa fa-map-marker"></i> ' . $infrastructureNonObject['address']; ?></p>
                <div class="map">
                    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_option( 'beplustheme_gmaps_api_key' ); ?>"></script>
                    <div id="gmap" class="gmap"></div> 
                    <script type="text/javascript"> 
                        var myLatlng = new google.maps.LatLng(<?php echo $location['lat'] . ',' . $location['lng']; ?>);					
                        var myOptions = {
                            zoom: 14,
                            center: myLatlng,
                        };
                        var map = new google.maps.Map(document.getElementById("gmap"), myOptions);
                        var contentString = '<div id="content">'+
                              '<div id="siteNotice">'+
                              '</div>'+
                              '<h4 id="firstHeading" class="firstHeading"><?php the_title(); ?></h4>'+
                              '<div id="bodyContent">'+
                              '<p><?php echo htmlspecialchars($location['address'], ENT_QUOTES); ?></p>'+
                              '</div>'+
                              '</div>';
                        var infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            icon: image,
                            map: map,
                            title: "<?php the_title(); ?>"
                        });
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.open(map,marker);
                        });
                    </script> 
				</div>
			<?php
		} */

		if( $activityInfo ) {
			?>
			<div class="activity-info-field">
				<span class="activity-info-data"><?php echo $activityInfo; ?></span>
			</div>
			<?php
		} elseif( $postContent = get_post_field( 'post_content', $postID ) ) {
			?>
				<span class="activity-post-content"><?php echo $postContent; ?></span>
			<?php
		}

	}

}

/* Function to build the activity days table in single-activity */
if ( ! function_exists( 'buildActivityDaysTable' ) ) {

	function buildActivityDaysTable( $postID ) {

		$daysHours = array();
		if( have_rows( 'activity_date_repeater', $postID ) ) :
			$index = 0;
			while( have_rows( 'activity_date_repeater', $postID ) ) {
				the_row();
				$day = get_sub_field( 'repeater_activity_day', $postID );
				$start = substr( get_sub_field( 'repeater_activity_start_time', $postID ), 0, -3 );
				$end = substr( get_sub_field( 'repeater_activity_end_time', $postID ), 0, -3 );
				$allDay = get_sub_field( 'activity_all_day', $postID ) ? [__( 'All Day', 'beplustheme' )] : '';
				$infrastructure = get_sub_field( 'repeater_activity_infrastructure', $postID ) ? get_sub_field( 'repeater_activity_infrastructure', $postID ) : '';
				$displayAge = get_sub_field( 'repeater_activity_display_age' );
				$level = get_sub_field( 'repeater_activity_level' );
				$hourString = $start.' - '.$end;

				/* if( empty( $daysHours ) ) {
					if( $allDay ) {
						$daysHours[$day] = $allDay;
					} else {
						$daysHours[$day] = $hourArray;
					}
				} else {
					if( array_key_exists( $day, $daysHours ) ) {
						$daysHours[$day][] = $hourArray[0];
					} else {
						if( $allDay ) {
							$daysHours[$day] = $allDay;
						} else {
							$daysHours[$day] = $hourArray;
						}
					}
				} */
				if( $infrastructure ) {
					$infrastructureString = '<a class="infrastructure-link" href="'. get_permalink( $infrastructure->ID ) .'">'. $infrastructure->post_title .'</a>';
				}
				$daysHours[$index]['day'] = $day;
				$daysHours[$index]['hour'] = $hourString;
				$daysHours[$index]['infrastructure'] = $infrastructureString;
				$daysHours[$index]['displayAge'] = $displayAge;
				$daysHours[$index]['level'] = $level;

				$index++;
			}
		endif;

		if( !empty( $daysHours ) ) {
			/* $monday = __( 'Monday', 'beplustheme' );
			$tuesday = __( 'Tuesday', 'beplustheme' );
			$wednesday = __( 'Wednesday', 'beplustheme' );
			$thursday = __( 'Thursday', 'beplustheme' );
			$friday = __( 'Friday', 'beplustheme' );
			$saturday = __( 'Saturday', 'beplustheme' );
			$sunday = __( 'Sunday', 'beplustheme' );
			$weekDays = [$monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday];
			$classArray = [];
			foreach ( $weekDays as $day ) {
				if( $daysHours[$day] ) {
					$classArray[$day] = 'activity-hours';
				} else {
					$classArray[$day] = 'no-activity-hours';
				}
			} */
			?>
				<!-- <div class="activity-days">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<td><?php echo $monday; ?></td>
									<td><?php echo $tuesday; ?></td>
									<td><?php echo $wednesday; ?></td>
									<td><?php echo $thursday; ?></td>
									<td><?php echo $friday; ?></td>
									<td><?php echo $saturday; ?></td>
									<td><?php echo $sunday; ?></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php foreach( $classArray as $weekDay => $class ) : ?>
										<td class="<?php echo $class; ?>">
											<?php if( $class != 'no-activity-hours' ) : ?>
												<?php foreach( $daysHours[$weekDay] as $hour ) : ?>
													<?php echo $hour; ?>
													<br/>
												<?php endforeach; ?>
											<?php endif; ?>
										</td>
									<?php endforeach; ?>
								</tr>
							</tbody>
						</table>
					</div>
				</div> -->

				<div class="activity-days">
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<td><?php _e( 'Day', 'beplustheme' ); ?></td>
									<td><?php _e( 'Hours', 'beplustheme' ); ?></td>
									<td><?php _e( 'Infrastructure', 'beplustheme' ); ?></td>
									<td><?php _e( 'Level', 'beplustheme' ); ?></td>
									<td><?php _e( 'Age', 'beplustheme' ); ?></td>
								</tr>
							</thead>
							<tbody>
								<?php foreach( $daysHours as $day ) : ?>
									<tr>
										<td class="day">
											<?php echo $day['day']; ?>
										</td>
										<td class="hours">
											<?php echo $day['hour']; ?>
										</td>
										<td class="infrastructure">
											<?php echo $day['infrastructure']; ?>
										</td>
										<td class="level">
											<?php echo $day['level']; ?>
										</td>
										<td class="age">
											<?php echo $day['displayAge']; ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>

			<?php
		}

	}

}

/* Function to dynamically populate excerpt if exists content */
if ( ! function_exists( 'populateExcerpt' ) ) {
	
	function populateExcerpt( $data ) {
		global $post;
		
		$content = '';
		$flexibleContents = array( 'first', 'second', 'third' );

		if( !get_the_excerpt( $data ) ) {
			if( $post->post_content != '' ) {
				$content = $post->post_content;
			} else if ( $post->post_type == 'activity' && get_field( 'activity_info' ) ) {
				$content = get_field( 'activity_info' );
			} else {
				foreach( $flexibleContents as $flexSection ) {
					if ( have_rows( $post->post_type.'_'.$flexSection.'_flexible' ) ) {
						while ( have_rows( $post->post_type.'_'.$flexSection.'_flexible' ) ) {
							the_row();
							$layout = get_row_layout();
							if( $layout === 'content' ) {
								$content = get_sub_field( $layout.'_text' );
								break;
							} else if( $layout === 'content_two_cols' ) {
								$content = get_sub_field( 'content_first_column_text' );
								break;
							} else if( $layout === 'content_three_cols' ) {
								$content = get_sub_field( 'content_three_cols_first_column_text' );
								break;
							}
						}
					}
				}
			}
			$excerpt = wp_trim_words( $content, 30, '...' );
			$postUpdate = array(
				'ID' => $post->ID,
				'post_excerpt' => $excerpt,
			);
			wp_update_post( $postUpdate );
		}

	}
	add_action( 'acf/save_post', 'populateExcerpt' );

}

/* Function to allow wildcards in WP_Query */
if ( ! function_exists( 'allowWildcards' ) ) {

	function allowWildcards( $where ) {  
		global $wpdb;
		$where = preg_replace("/(.+)(={1})(.+%.+)/", "$1 LIKE $3", $wpdb->remove_placeholder_escape( $where ) );
		return $where;
	}
	add_filter( 'posts_where' , 'allowWildcards' );

}

/* Function to check if a string starts with a certain substring */
if ( ! function_exists( 'startsWith' ) ) {

	function startsWith( $string, $startString ) { 

		$len = strlen( $startString ); 
		return ( substr( $string, 0, $len ) === $startString ); 

	}

}

/* Missing template warning (only for admins) */
if ( ! function_exists( 'bpsMissingTemplateWarning' ) ) {

	function bpsMissingTemplateWarning( $templateName ) {
		if( current_user_can( 'administrator' ) ) {
            echo sprintf( __( 'ADMIN WARNING: Template <b>%s</b> doesn\'t exist. Please change the template name and try again.', 'beplustheme' ), $templateName );
        }
	}

}

/* Needed post type not active warning (only for admins) */
if ( ! function_exists( 'bpsNeededPostTypeNotActiveWarning' ) ) {

	function bpsNeededPostTypeNotActiveWarning( $postType ) {
		if( current_user_can( 'administrator' ) ) {
            echo sprintf( __( 'ADMIN WARNING: Post type %s is not active. Please activate it and try again.', 'beplustheme' ), $postType );
        }
	}

}

/*
 * VUE Aux functions 
 */

if ( ! function_exists( 'vueSortMultiarrayTreeselect' ) ) {

	function vueSortMultiarrayTreeselect( $a, $b ) {

		return strcmp( $a['label'], $b['label'] );

	}
	
}

if ( ! function_exists( 'vueRemoveChildrenPostsFromTreeselect' ) ) {

	function vueRemoveChildrenPostsFromTreeselect( $allPosts, $allChildrenPosts ) {

		foreach( $allPosts as $key => $post ) {
			if( in_array( $post['id'], $allChildrenPosts ) ) {
				unset( $allPosts[$key] );
			}
		}
		return $allPosts;

	}
}

if( ! function_exists( 'populateDaysAndHoursTaxonomiesForActivitySearch' ) ) {

	function populateDaysAndHoursTaxonomiesForActivitySearch( $postId ) {
		if( $_POST['post_type'] == 'activity' ) {
			$postType = $_POST['post_type'];
			$repeaterFields = get_field( $postType . '_date_repeater' );
			if( !empty( $repeaterFields ) ) {
				$allDaysHours = array();
				foreach( $repeaterFields as $field ) {
					$day = translateDayToEnglish( strtolower( $field['repeater_' . $postType . '_day'] ) );
					$startTime = date( 'H', strtotime( $field['repeater_' . $postType . '_start_time'] ) );
					$activityId = get_term_by( 'slug', $day . '_' . $startTime, 'activitydays' )->term_id;
					$allDaysHours[] = $activityId;
				}
				wp_set_post_terms( $postId, $allDaysHours, 'activitydays' );
			}
		}

	}
	add_action( 'acf/save_post', 'populateDaysAndHoursTaxonomiesForActivitySearch', 10 );

}

if( ! function_exists( 'translateDayToEnglish') ) {

	function translateDayToEnglish( $day ) {

		$locale = get_locale();
		if( $locale == 'fr_FR' ) {
			switch( $day ) {
				case 'lundi':
					$day = 'monday';
					break;
				case 'mardi':
					$day = 'tuesday';
					break;			
				case 'mercredi':
					$day = 'wednesday';
					break;
				case 'jeudi':
					$day = 'thursday';
					break;
				case 'vendredi':
					$day = 'wednesday';
					break;
				case 'samedi':
					$day = 'saturday';
					break;
				case 'dimanche':
					$day = 'sunday';
					break;
			}
		}
		return $day;

	}

}

/* VUE Activity Search Form */
if ( ! function_exists( 'addActivityInfoToQueriedPosts' ) ) {

	function addActivityInfoToQueriedPosts( $posts, $selectedAge ) {
		foreach( $posts as $post ) {
			$thumbnail_url = get_the_post_thumbnail_url( $post->ID );
			if( $thumbnail_url == '' || is_null( $thumbnail_url ) ) {
				$postType = get_post_type( $post->ID );
				$optionName = $postType.'_single_background_image';
				$thumbnail_url = wp_get_attachment_image_src( get_option( $optionName ), 'full' )[0];
			}
			$post->thumbnail_url = $thumbnail_url;
			$post->post_url = get_permalink( $post->ID );
	
			/* Add Activity categories */
			$selectedCategoriesMeta = get_the_terms( $post->ID, 'activitycat' );
			$categories = array();
			foreach( $selectedCategoriesMeta as $category ) {
				$categoryInfo = array( 
					'name' => $category->name,
					'slug' => $category->slug,
					'taxonomy' => $category->taxonomy
				);
				 $categories[] = $categoryInfo;
			}
			$post->post_categories = $categories;
			
			/* Add Activity sport */
			if( get_option( 'beplustheme_display_sport' ) ) {
				if( get_post_meta( $post->ID, 'activity_check_sport' )[0] ) {
					$selectedSport = get_post_meta( $post->ID, 'activity_sport' )[0];
					$sportInfo = array( 
						'name' => get_the_title( $selectedSport ),
						'link' => get_permalink( $selectedSport )
					);
				} else {
					$selectedSport = get_post_meta( $post->ID, 'activity_sport_name' );
					$sportInfo = array(
						'name' => $selectedSport,
						'link' => ''
					);
				}
				$post->linked_sport = $sportInfo;
			}

			/* Add Activity club */
			if( get_option( 'beplustheme_display_club' ) ) {
				if( get_post_meta( $post->ID, 'activity_check_club' )[0] ) {
					$selectedClub = get_post_meta( $post->ID, 'activity_club' )[0];
					$clubInfo = array( 
						'name' => get_the_title( $selectedClub ),
						'link' => get_permalink( $selectedClub )
					);
				} else {
					$selectedClub = get_post_meta( $post->ID, 'activity_club_name' );
					$clubInfo = array(
						'name' => $selectedClub,
						'link' => ''
					);
				}
				$post->linked_club = $clubInfo;
			}

			/* Add Activity infrastructure */
			if( get_post_meta( $post->ID, 'activity_check_infrastructure' )[0] ) {
				$selectedInfrastructure = get_post_meta( $post->ID, 'activity_infrastructure' )[0];
				$infrastructureInfo = array( 
					'name' => get_the_title( $selectedInfrastructure ),
					'link' => get_permalink( $selectedInfrastructure )
				);
			} else {
				$selectedInfrastructure = get_post_meta( $post->ID, 'activity_location_name' );
				$infrastructureInfo = array(
					'name' => $selectedInfrastructure,
					'link' => ''
				);
			}
			$post->linked_infrastructure = $infrastructureInfo;
			
			/* Add Activity hours */
			if( $selectedAge ) {
				if( get_post_meta( $post->ID, 'activity_group_by_day' )[0] ) {
					$activityDays = getActivityDatesGroupedByDays( $post->ID, $selectedAge, 0 );
				} else {
					$activityDays = getActivityDatesGroupedByHours( $post->ID, $selectedAge, 0 );
				}
			} else {
				if( get_post_meta( $post->ID, 'activity_group_by_day' )[0] ) {
					$activityDays = getActivityDatesGroupedByDays( $post->ID, null, 1 );
				} else {
					$activityDays = getActivityDatesGroupedByHours( $post->ID, null, 1 );
				}
			}

			/* write_log( $activityDays ); */
			$post->activity_days = $activityDays;

			if( empty( $activityDays ) ) {
				unset( $posts[$post] );
			}
		}
	}
}

if( ! function_exists( 'getActivityDatesGroupedByDays' ) ) {

	function getActivityDatesGroupedByDays( $postID, $selectedAge = null, $notFromQuery = 1 ) {
		$activityDays = array();
		$repeatedContent;

		$daysRepeater = get_post_meta( $postID, 'activity_date_repeater', true );
		if( $daysRepeater ) {
			for( $i=0; $i<$daysRepeater; $i++ ) {
				$day = __( get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_day', true ), 'beplustheme' );
				$start = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_start_time', true );
				$end = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_end_time', true );
				$allDay = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_activity_all_day', true ) ? __( 'All Day', 'beplustheme' ) : '';
				$dayAgeFrom = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_from', true ) ? get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_from', true ) : 0;
				$dayAgeTo = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_to', true ) ? get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_to', true ) : 99;

				if( in_array( $selectedAge, range( $dayAgeFrom, $dayAgeTo ) ) || $notFromQuery ) {

					if( empty( $activityDays ) ) {
						if( $allDay ) {
							$activityDays[$day] = $allDay;	
						} else {
							$activityDays[$day] = [' '.__( 'from', 'beplustheme' ).' '.date( 'H:i', strtotime( $start ) ).' '.__( 'to', 'beplustheme' ).' '.date( 'H:i', strtotime( $end ) )];
						}
					} else {
						$hours = ' '.__( 'from', 'beplustheme' ).' '.date( 'H:i', strtotime( $start ) ).' '.__( 'to', 'beplustheme' ).' '.date( 'H:i', strtotime( $end ) );
						if( isset( $activityDays[$day] ) ) {
							if( !in_array( $hours, $activityDays[$day] ) ) {
								$activityDays[$day][] = $hours;
							}
						} else {
							if( $allDay ) {
								$activityDays[$day] = $allDay;
							} else {
								$activityDays[$day] = [$hours];
							}
						}
					}

				}

			}
		}
		if( $repeatedContent ) {
			foreach( $repeatedContent as $key => $value ) {
				$activityDays[$key] = $value;
			}
		}
		return $activityDays;
	}

}

if( ! function_exists( 'getActivityDatesGroupedByHours' ) ) {

	function getActivityDatesGroupedByHours( $postID, $selectedAge = null, $notFromQuery = 1 ) {
		$activityDays = array();
		$hoursActivity = array();
		$repeatedContent;

		$daysRepeater = get_post_meta( $postID, 'activity_date_repeater', true );
		if( $daysRepeater ) {
			for( $i=0; $i<$daysRepeater; $i++ ) {
				$day = __( get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_day', true ), 'beplustheme' );
				$start = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_start_time', true );
				$end = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_end_time', true );
				$allDay = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_activity_all_day', true ) ? __( 'All Day', 'beplustheme' ) : '';
				$dayAgeFrom = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_from', true ) ? get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_from', true ) : 0;
				$dayAgeTo = get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_to', true ) ? get_post_meta( $postID, 'activity_date_repeater_'.$i.'_repeater_activity_age_to', true ) : 99;

				if( in_array( $selectedAge, range( $dayAgeFrom, $dayAgeTo ) ) || $notFromQuery ) {
				
					if( empty( $activityDays ) && empty( $hoursActivity ) ) {
						if( $allDay ) {
							$activityDays[$day] = $allDay;	
						} else {
							$activityDays[$day] = [' '.__( 'from', 'beplustheme' ).' '.date( 'H:i', strtotime( $start ) ).' '.__( 'to', 'beplustheme' ).' '.date( 'H:i', strtotime( $end ) )];
							$hoursActivity[' '.__( 'from', 'beplustheme' ).' '.date( 'H:i', strtotime( $start ) ).' '.__( 'to', 'beplustheme' ).' '.date( 'H:i', strtotime( $end ) )] = $day;
						}
					} else {
						$hours = ' '.__( 'from', 'beplustheme' ).' '.date( 'H:i', strtotime( $start ) ).' '.__( 'to', 'beplustheme' ).' '.date( 'H:i', strtotime( $end ) );
						if( isset( $activityDays[$day] ) ) {
							if( !in_array( $hours, $activityDays[$day] ) ) {
								$activityDays[$day][] = $hours;
							}
							if( isset( $hoursActivity[$hours] ) ) {
								$hourKey = $hoursActivity[$hours];
								if( isset( $activityDays[$hourKey] ) ) {
									$dayHourKey = array_search( $hours, $activityDays[$hourKey] );
									unset( $activityDays[$hourKey][$dayHourKey] );
									if( empty( $activityDays[$hourKey] ) ) {
										unset( $activityDays[$hourKey] );
									}
									if( $activityDays[$day] && in_array( $hours, $activityDays[$day] ) ) {
										$dayHourKey = array_search( $hours, $activityDays[$day] );
										unset( $activityDays[$day][$dayHourKey] );
									}
									if( empty( $activityDays[$day] ) ) {
										unset( $activityDays[$day] );
									}
									$newDayKey = $hourKey.', '.$day;
									if( $activityDays[$newDayKey] ) {
										$repeatedContent[$newDayKey] = [$hours];
									}
									$activityDays[$newDayKey] = [$hours];
									$hoursActivity[$hours] = $newDayKey;
								} else {
									$activityDays[$day][] = $hours;
								}
							}
							$hoursActivity[$hours] = $day;
						} else if( isset( $hoursActivity[$hours] ) ) {
							$hourKey = $hoursActivity[$hours];
							if( isset( $activityDays[$hourKey] ) ) {
								$dayHourKey = array_search( $hours, $activityDays[$hourKey] );
								unset( $activityDays[$hourKey][$dayHourKey] );
								if( empty( $activityDays[$hourKey] ) ) {
									unset( $activityDays[$hourKey] );
								}
								if( $activityDays[$day] && in_array( $hours, $activityDays[$day] ) ) {
									$dayHourKey = array_search( $hours, $activityDays[$day] );
									unset( $activityDays[$day][$dayHourKey] );
								}
								if( empty( $activityDays[$day] ) ) {
									unset( $activityDays[$day] );
								}
								$newDayKey = $hourKey.', '.$day;
								if( $activityDays[$newDayKey] ) {
									$repeatedContent[$newDayKey] = [$hours];
								}
								$activityDays[$newDayKey] = [$hours];
								$hoursActivity[$hours] = $newDayKey;
							} else {
								$activityDays[$day][] = $hours;
							}
						} else {
							if( $allDay ) {
								$activityDays[$day] = $allDay;
							} else {
								$activityDays[$day] = [$hours];
								$hoursActivity[$hours] = $day;
							}
						}
					}

				}
			}
		}
		if( $repeatedContent ) {
			foreach( $repeatedContent as $key => $value ) {
				$activityDays[$key] = $value;
			}
		}
		return $activityDays;
	}

}

/* Create Activity Search Form shortcode */
if( ! function_exists( 'vueActivitySearchForm' ) ) {

	function vueActivitySearchForm() {

		ob_start();
		\BPS\BePlusPlugin\Helpers\BePlusPluginHelpers::loadTemplate( 'public/templates/activitySearchForm.php', array(), false );
		return ob_get_clean();

	}
	add_shortcode( 'activity_search_form', 'vueActivitySearchForm' );

}

if( ! function_exists( 'vueActivitySearchFormHome' ) ) {

	function vueActivitySearchFormHome() {

		ob_start();
		\BPS\BePlusPlugin\Helpers\BePlusPluginHelpers::loadTemplate( 'public/templates/activitySearchFormHome.php', array(), false );
		return ob_get_clean();

	}
	add_shortcode( 'activity_search_form_home', 'vueActivitySearchFormHome' );

}

if( ! function_exists( 'localizeVarsForVue' ) ) {

	function localizeVarsForVue() {

		$textDomainTranslations = get_translations_for_domain('beplustheme');
		$vueTranslation = [];
		foreach($textDomainTranslations->entries as $key => $Object) {
		$vueTranslation[$key] = __($key, 'beplustheme');
		}

		$vueVars = [
			'ajaxUrl' => admin_url( 'admin-ajax.php' )
		];
		wp_localize_script( 'beplustheme-public', 'vueVars', $vueVars );
		wp_localize_script( 'beplustheme-public', 'vueTranslation', $vueTranslation );

	}
	add_action( 'wp_head', 'localizeVarsForVue', 1 );

}

if( ! function_exists( 'getActivityFormDisplayOptions' ) ) {

	function getActivityFormDisplayOptions() {

		$optionsArray = array(
			'displaySport' => get_option( 'beplustheme_display_sport' ),
			'displayClub' => get_option( 'beplustheme_display_club' ),
		);
		$return['displayOptions'] = $optionsArray;
		wp_send_json_success( $return );

	}
	add_action( 'wp_ajax_nopriv_display_options_vue_form', 'getActivityFormDisplayOptions' );
	add_action( 'wp_ajax_display_options_vue_form', 'getActivityFormDisplayOptions' );

}

/* if( ! function_exists( 'getActivityAcfFieldsForSearchForm' ) ) {

	function getActivityAcfFieldsForSearchForm() {

		$selectedPostType = json_decode( html_entity_decode( stripslashes ( $_POST['body'] ) ) )->selectedPostType;
		if( $selectedPostType == 'activity' ) {
			$cptFields = array();
			$groupField = acf_get_field_groups( array( 'post_type' => $selectedPostType ) )[0];
			$allFields = acf_get_fields( $groupField['key'] );
			foreach( $allFields as $field ) {
				if( $field['type'] == 'repeater' ) {
					foreach( $field['sub_fields'] as $repeaterField ) {
						if( $repeaterField['type'] != 'true_false' ) {
							$cptFields[] = ['value' => $repeaterField['name'], 'text' => $repeaterField['label']];
						}
					}
				} elseif( $field['type'] != 'true_false' ) {
					$cptFields[] = ['value' => $field['name'], 'text' => $field['label']];
				}
			}
			$return['inputActivityAcf'] = $cptFields;
		} else {
			$return['inputActivityAcf'] = '';
		}
		wp_send_json_success( $return );

	}
	add_action( 'wp_ajax_nopriv_activity_acf_fields_vue_form', 'getActivityAcfFieldsForSearchForm' );
	add_action( 'wp_ajax_activity_acf_fields_vue_form', 'getActivityAcfFieldsForSearchForm' );
} */

if( ! function_exists( 'vueGetPostTaxonomies' ) ) {

	function vueGetPostTaxonomies() {

		$postTaxonomy = json_decode( html_entity_decode( stripslashes ( $_POST['body'] ) ) )->postTaxonomy;
		$allTaxonomies = array();
		$terms = get_terms( array( 
			'taxonomy' => $postTaxonomy,
			'hide_empty' => false,
		) );
		foreach( $terms as $term ) {
			$allTaxonomies[] = ['id' => $term->term_id, 'label' => $term->name];
		}
		$return['taxFields'] = $allTaxonomies;
		wp_send_json_success( $return );

	}
	add_action( 'wp_ajax_nopriv_post_taxonomies_vue_form', 'vueGetPostTaxonomies' );
	add_action( 'wp_ajax_post_taxonomies_vue_form', 'vueGetPostTaxonomies' );

}

if( ! function_exists( 'getPostFieldsForSearchForm' ) ) {

	function getPostFieldsForSearchForm() {

		$postType = json_decode( html_entity_decode( stripslashes ( $_POST['body'] ) ) )->postType;
		$allPosts = array();
		$args = array(
			'post_type' => $postType,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'fields' => 'ids',
		);
		$posts = query_posts( $args );
		if( $postType == 'sport' ) {
			$allChildrenPosts = array();
			foreach( $posts as $post ) {
				$childrenArray = get_children( array(
					'post_parent' => $post,
					'post_type' => $postType,
					'numberposts' => -1,
					'post_status' => 'publish',
					'fields' => 'ids',
				) );
				if( !empty( $childrenArray ) ) {
					$childrenSports = array();
					foreach( $childrenArray as $childrenPost ) {
						$childrenPostArray = array(
							'id' => $childrenPost,
							'label' => get_the_title( $childrenPost ),
						);
						$allChildrenPosts[] = $childrenPost;
						$childrenSports[] = $childrenPostArray;
						usort( $childrenSports, 'vueSortMultiarrayTreeselect' );
					}
					$allPosts[] = ['id' => $post, 'label' => get_the_title( $post ), 'children' => $childrenSports];
				} else {
					$allPosts[] = ['id' => $post, 'label' => get_the_title( $post )];
				}
			}
			$allPosts = vueRemoveChildrenPostsFromTreeselect( $allPosts, $allChildrenPosts );
			usort( $allPosts, 'vueSortMultiarrayTreeselect' );
			array_unshift( $allPosts, ['id' => 'all', 'label' => __( 'All', 'beplustheme' )] );
		} elseif ( $postType == 'club' ) {
			foreach( $posts as $post ) {
				$allPosts[] = ['id' => $post, 'label' => get_the_title( $post )];
			}
			usort( $allPosts, 'vueSortMultiarrayTreeselect' );
			array_unshift( $allPosts, ['id' => 'all', 'label' => __( 'All', 'beplustheme' )] );
		}
		$return['postFields'] = $allPosts;
		wp_send_json_success( $return );

	}
	add_action( 'wp_ajax_nopriv_post_fields_vue_form', 'getPostFieldsForSearchForm' );
	add_action( 'wp_ajax_post_fields_vue_form', 'getPostFieldsForSearchForm' );

}

if( ! function_exists( 'activitySearchFormPost' ) ) {

	function activitySearchFormPost() {

		$formData = json_decode( html_entity_decode( stripslashes ( $_POST['body'] ) ) )->formData;
		$page = json_decode( html_entity_decode( stripslashes ( $_POST['body'] ) ) )->currentPage;
		$posts_per_page = json_decode( html_entity_decode( stripslashes ( $_POST['body'] ) ) )->perPage;
		$postType = strtolower( $formData->selectedPostType );
		/* if( !$formData->selectedAge ) {
			$formData->selectedAgeFrom = 0;
		}
		if( !$formData->selectedAgeTo ) {
			$formData->selectedAgeTo = 99;
		} */
		$queryArgs = array(
			'post_type' => $postType,
			'posts_per_page' => $posts_per_page,
			'order' => 'ASC',
			'orderby' => 'title',
			'paged' => $page,
		);
		/* Select Day (if filled) */
		if( !$formData->selectedAge ) {
			$metaQuery = array(
				'relation' => 'AND',
			);
		} else {
			$metaQuery = array(
				'relation' => 'AND',
				array(
					'relation' => 'AND',
					array(
						'key' => $postType . '_age_from',
						'value' => $formData->selectedAge,
						'compare' => '<=',
						'type' => 'NUMERIC',
					),
					array(
						'key' => $postType . '_age_to',
						'value' => $formData->selectedAge,
						'compare' => '>=',
						'type' => 'NUMERIC',
					),
				),
			);
		}
		$taxQuery = array(
			'relation' => 'OR',
		);
		/* Select Categories */
		if( !empty( $formData->selectedPeriodField ) ) {
			$categories = array();
			foreach( $formData->selectedPeriodField as $period ) {
				$categories[] = $period;
			}
			$periodQuery = array(
				'taxonomy' => 'activitycat',
				'field' => 'term_id',
				'terms' => $categories,
			);
			$taxQuery[] = $periodQuery;	
		}
		/* Select Taxonomies */
		if( !empty( $formData->selectedLevelField ) ) {
			$levels = array();
			foreach( $formData->selectedLevelField as $level ) {
				$levels[] = $level;
			}
			$levelQuery = array(
				'taxonomy' => 'levelcat',
				'field' => 'term_id',
				'terms' => $levels,
			);
			$taxQuery[] = $levelQuery;
		}
		/* Select Days and Hours */
		if( !empty( (array)$formData->daysHoursItems ) ) {
			$notNullDaysHoursItems = false;
			foreach( (array)$formData->daysHoursItems as $daysArray ) {
				if( !empty( $daysArray ) ) {
					foreach( $daysArray as $hours ) {
						if( $hours ) {
							$notNullDaysHoursItems = true;
							break;
						}
					}
					if( $notNullDaysHoursItems ) {
						break;
					}
				}
			}
			if( $notNullDaysHoursItems ) {
				$dayTime = array();
				$morning = range( get_option('beplustheme_morning_hour'), get_option('beplustheme_afternoon_hour') );
				$afternoon = range( get_option('beplustheme_afternoon_hour'), get_option('beplustheme_evening_hour') );
				if( get_option('beplustheme_finish_hour') != 00 ) {
					$evening = range( get_option('beplustheme_evening_hour'), get_option('beplustheme_finish_hour') );
				} else {
					$evening = range( get_option('beplustheme_evening_hour'), 23 );
					$evening[] = '00';
				}
				foreach( $formData->daysHoursItems as $day => $time ) {
					if( !empty( $time ) ) {
						if( $time->morning ) {
							foreach( $morning as $hour ) {
								$dayTime[] = $day . '_' .$hour;
							}
						}
						if( $time->afternoon ) {
							foreach( $afternoon as $hour ) {
								$dayTime[] = $day . '_' .$hour;
							}
						}
						if( $time->evening ) {
							foreach( $evening as $hour ) {
								$dayTime[] = $day . '_' .$hour;
							}
						}
					}
				}
				$dayTimeQuery = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'activitydays',
						'field' => 'slug',
						'terms' => $dayTime,
					),
				);
				$taxQuery[] = $dayTimeQuery;
			}
		}
		/* Select Sport */
		if( !empty( $formData->selectedSportField ) ) {
			$sportFieldQuery = array(
				'relation' => 'OR',
			);
			foreach( $formData->selectedSportField as $sport ) {
				if( $sport != 'all' ) {
					$sportQuery = array(
						'key' => $postType . '_sport',
						'value' => $sport,
						'compare' => '=',
					);
					$sportFieldQuery[] = $sportQuery;
				}
				/* QUERY WHEN 'all' SPORTS */
			}
			$metaQuery[] = $sportFieldQuery;
		}
		/* Select Infrastructure */
		if( !empty( $formData->selectedInfrastructureField ) ) {
			$infrastructureFieldQuery = array(
				'relation' => 'OR',
			);
			foreach( $formData->selectedInfrastructureField as $infrastructure ) {
				if( $infrastructure != 'all' ) {
					$infrastructureQuery = array(
						'key' => $postType . '_infrastructure',
						'value' => $infrastructure,
						'compare' => '=',
					);
					$infrastructureFieldQuery[] = $infrastructureQuery;
				}
				/* QUERY WHEN 'all' INFRASTRUCTURES */
			}
			$metaQuery[] = $infrastructureFieldQuery;
		}
		/* Select Club */
		if( !empty( $formData->selectedClubField ) ) {
			$clubFieldQuery = array(
				'relation' => 'OR',
			);
			foreach( $formData->selectedClubField as $club ) {
				if( $club != 'all' ) {
					$clubQuery = array(
						'key' => $postType . '_club',
						'value' => $club,
						'compare' => '=',
					);
					$clubFieldQuery[] = $clubQuery;
				}
				/* QUERY WHEN 'all' CLUBS */
			}
			$metaQuery[] = $clubFieldQuery;
		}

		$queryArgs['meta_query'] = $metaQuery;
		$queryArgs['tax_query'] = $taxQuery;
		/* write_log($queryArgs); */

		$query = new WP_Query( $queryArgs );
		$posts = $query->get_posts();
		addActivityInfoToQueriedPosts( $posts, $formData->selectedAge );

		$return['posts'] = $query->posts;
		$return['found_posts'] = $query->found_posts;
		wp_send_json_success ( $return );

	}
	add_action( 'wp_ajax_nopriv_activity_search_form_post', 'activitySearchFormPost' );
	add_action( 'wp_ajax_activity_search_form_post', 'activitySearchFormPost' );

}