<?php
/**
 * The template for displaying all single posts.
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'beplustheme_container_type' );
?>

<div class="wrapper" id="site-content-wrapper">

	<div id="site-content">

		<div id="site-content-container">

			<div class="<?php echo esc_attr( $container ) . ' ' . get_field( get_post_type() . '_post_class' ); ?>" id="content" tabindex="-1">

				<div class="row">

					<!-- Do the left sidebar check -->
					<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

					<main class="site-main" id="main">

						<?php while ( have_posts() ) : the_post(); ?>

							<?php get_template_part( 'loop-templates/content', 'single' ); ?>

								<?php beplustheme_post_nav(); ?>

							<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							?>

							<?php
							/* Get sections fields */
							$flexible_first_section = get_post_type() . 'first_flexible';
							$flexible_second_section = get_post_type() .'second_flexible';
							$flexible_third_section = get_post_type() .'third_flexible';

							/* Check if the first section flexible content field has rows of data */
							if ( have_rows( $flexible_first_section ) ):

								/* Loop through the rows of data */
								while ( have_rows( $flexible_first_section ) ) : the_row();

									\BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() );

								endwhile;

							endif;

							/* Check if the second section flexible content field has rows of data */
							if ( have_rows( $flexible_second_section ) ):

								/* Loop through the rows of data */
								while ( have_rows( $flexible_second_section ) ) : the_row();

									\BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() );

								endwhile;

							endif;

							/* Check if the third section flexible content field has rows of data */
							if ( have_rows( $flexible_third_section ) ):

								/* Loop through the rows of data */
								while ( have_rows( $flexible_third_section ) ) : the_row();

									\BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() );

								endwhile;

							endif;

							/* Load linked posts template */
							$allCpt = \BPS\BePlusPlugin\BePlusPlugin::getLoadedCpt();
							if( in_array( $post->post_type, $allCpt ) ) :
								$currentCpt = new $allCpt[ $post->post_type ]();
								$relationships = $currentCpt->getCptRelationshipsSlugs();
								if( isset( $relationships ) ) {
									$params = [
										'relationships' => $relationships,
										'cptSlug' => $post->post_type,
									];
									\BPS\BePlusPlugin\Helpers\BePlusPluginHelpers::loadTemplate( 'public/templates/relationships.php', $params, false );
								}
							endif;

							?>


						<?php endwhile; // end of the loop. ?>

					</main><!-- #main -->

				<!-- Do the right sidebar check -->
				<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

			</div><!-- .row -->

		</div>

	</div>

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
