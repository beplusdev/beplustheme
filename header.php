<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'beplustheme_container_type' );
$archiveImage = get_option( get_post_type() . '_archive_background_image' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<?php if( $customCss = get_option( 'beplustheme_custom_css' ) ) : ?>
		<style>
			<?php echo $customCss; ?>
		</style>
	<?php endif; ?>
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<?php if( is_archive() && $archiveImage ) : ?>
		<div id="site-header-wrapper-image" style="background:linear-gradient(to bottom,white,rgba(255,255,255,0.5)),url('<?php echo wp_get_attachment_image_src( $archiveImage, 'full' )[0]; ?>') no-repeat center;"" itemscope itemtype="http://schema.org/WebSite">
	<?php else : ?>
		<div id="site-header-wrapper" itemscope itemtype="http://schema.org/WebSite">
	<?php endif; ?>

		<div id="site-header">

			<div id="site-header-container">

				<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'beplustheme' ); ?></a>

				<nav id="logo-header" class="navbar navbar-expand-md navbar-dark bps-bg-header">

					<?php if ( 'container' == $container ) : ?>
						<div class="container" >
					<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() && ! beplustheme_options_exists_website_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } elseif ( has_custom_logo() && ! beplustheme_options_exists_website_logo() ) {
						the_custom_logo();
					} else {
						beplustheme_options_get_website_logo();
					} ?><!-- end custom logo -->

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'beplustheme' ); ?>">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="bps-subheader-info">
						<?php if( $contactPhone = get_option( 'beplustheme_contact_phone' ) ) : ?>
							<a target="_blank" href="tel:<?php echo str_replace( ".", "", str_replace( " ", "", $contactPhone ) ); ?>" class="bps-header-contact-phone-icon"><?php echo $contactPhone; ?></a>
						<?php endif; ?>
						<?php if( has_nav_menu( 'subheader-menu' ) ) : ?>
							<div class="bps-vl"></div>
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'subheader-menu',
									'container_class' => 'subheader-navmenu-container',
									'container_id'    => 'navbarSubmenuNavDropdown',
									'menu_class'      => 'navbar-nav ml-auto',
									'fallback_cb'     => '',
									'menu_id'         => 'subheader-menu',
									'depth'           => 2,
									'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
								)
							); ?>
						<?php endif; ?>
					</div>

					<?php if ( 'container' == $container ) : ?>
					</div><!-- .container -->
					<?php endif; ?>

				</nav><!-- .site-subheader-navigation -->

				<nav class="navbar navbar-expand-md navbar-dark bps-bg-header">
					<div class="container header-primary-menu">
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'collapse navbar-collapse bps-header-primary-menu-container',
								'container_id'    => 'navbarNavDropdown',
								'menu_class'      => 'navbar-nav bps-header-primary-menu',
								'fallback_cb'     => '',
								'menu_id'         => 'main-menu',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						); ?>
					</div>
				</nav><!-- .site-header-navigation -->
			
			</div>
			
		</div>

	</div><!-- #wrapper-header end -->

<?php beplustheme_load_header_image(); ?>
