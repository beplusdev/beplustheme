<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;
    
    // Get the image
    $imageUrl = bpsGetCptImageInTemplate( $postType, $postID );

    // Get the logo
    $logoUrl = bpsGetCptLogoInTemplate( $postType, $postID );
    
    // Get all activity parameters
    $sportObject = get_field( 'activity_sport', get_the_ID() );
    $sportNonObject = get_field( 'activity_sport_name', get_the_ID() );
    $clubObject = get_field( 'activity_club', get_the_ID() );
    $clubNonObject = get_field( 'activity_club_name', get_the_ID() );
    $activityCategories = get_the_terms( get_the_ID(), 'activitycat' );
    $activityDays = getActivityHoursInTemplate( get_the_ID() );
    $infrastructureObject = get_field( 'activity_infrastructure', get_the_ID() );
    $infrastructureNonObject = get_field( 'activity_location_name', get_the_ID() );
    $activityExcerpt = get_the_excerpt( $postID );

    // Get all display parameters
    $showSport = get_option( 'beplustheme_display_sport' );
    $showClub = get_option( 'beplustheme_display_club' );

    ?>

    <div class="bps-wrapper-card-activity <?php echo $displayOptions['classes']; ?>">
        <div class="card-block" data-aos="flip-left">
            <?php if( $imageUrl ) : ?>
                <div class="bps-card-img-wrapper">
                    <a class="bps-card-img-link" href="<? echo get_permalink( $postID ); ?>">
                        <img class="bps-card-img" src="<?php echo $imageUrl; ?>">
                    </a>
                </div>
            <?php endif; ?>
            <div class="bps-card-item">
                <a class="bps-item-link-button" href="<? echo get_permalink( $postID ); ?>">
                    <h4 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h4>
                </a>
                <?php if( ( $sportObject || $sportNonObject ) && $showSport ) : ?>
                    <div class="bps-item-card-sport">
                        <?php _e( 'Sport:', 'beplustheme' ); ?>
                        <?php if( $sportObject ) : ?>
                            <a href="<?php echo get_permalink( $sportObject->ID ); ?>"><?php echo $sportObject->post_title; ?></a>
                        <?php else: ?>
                            <?php echo $sportNonObject; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if( ( $clubObject || $clubtNonObject ) && $showClub ) : ?>
                    <div class="bps-item-card-club">
                        <?php _e( 'Club:', 'beplustheme' ); ?>
                        <?php if( $clubObject ) : ?>
                            <a href="<?php echo get_permalink( $clubObject->ID ); ?>"><?php echo $clubObject->post_title; ?></a>
                        <?php else: ?>
                            <?php echo $clubtNonObject; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if( $activityCategories ) : ?>
                    <div class="bps-item-card-period">
                        <?php foreach( $activityCategories as $category ) : ?>
                            <a class="period-link" href="<?php echo get_term_link( $category->term_id ); ?>"><?php echo $category->name; ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <div class="bps-item-card-date">
                    <?php if( count( $activityDays ) < 5 ) : ?>
                        <?php foreach ( $activityDays as $day => $hours ) : ?>
                            <span class="activity-days"><?php echo $day; ?>
                            <?php if( is_array( $hours ) ) : ?>
                                <?php foreach( $hours as $hour ) : ?>
                                    <?php echo $hour; ?>
                                    <?php if( end( $hours ) != $hour ) :?>
                                        <?php echo ', '; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php echo ' - '.$hours; ?>
                            <?php endif; ?>
                            </span>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <?php _e( 'Multiple days', 'beplustheme' ); ?>
                    <?php endif; ?>
                </div>
                <?php if( $infrastructureObject || $infrastructureNonObject ) : ?>
                    <div class="bps-item-card-infrastructure">
                        <?php if( $infrastructureObject ) : ?>
                            <a class="infrastructure-link" href="<?php echo get_permalink( $infrastructureObject->ID ); ?>"><?php echo $infrastructureObject->post_title; ?></a>
                        <?php else: ?>
                            <span class="infrastructure-link"><?php echo $infrastructureNonObject; ?></span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if( $activityExcerpt ) : ?>
                    <p class="bps-item-description"><?php echo $activityExcerpt; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/' . $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>