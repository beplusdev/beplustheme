<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;
    
    // Get the image
    $imageUrl = bpsGetCptImageInTemplate( $postType, $postID );

    // Get the logo
    $logoUrl = bpsGetCptLogoInTemplate( $postType, $postID );

    ?>

    <div class="bps-wrapper-card-overlay-text <?php echo $displayOptions['classes']; ?>">
        <div class="card-block">
            <img class="bps-card-img" src="<?php echo $imageUrl; ?>">
            <div class="card-img-overlay">
                <?php if( $logoUrl ) : ?>
                    <img class="bps-card-overlay-logo" src="<?php echo $logoUrl; ?>">
                <?php endif; ?>
                <h3 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h3>
                <p class="bps-item-description"><?php echo get_the_excerpt( $postID ) ? get_the_excerpt( $postID ) : '' ; ?></p>
                <a class="bps-item-link-button" href="<? echo get_permalink( $postID ); ?>"><?php _e( 'Read more', 'beplustheme-child' ); ?></a>
            </div>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/' . $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>