<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;
    
    // Get the image
    $imageUrl = bpsGetCptImageInTemplate( $postType, $postID );

    // Get the icon
    $iconUrl = bpsGetCptIconInTemplate( $postType, $postID );
    if( !$iconUrl ) {
        $iconUrl = bpsGetCptLogoInTemplate( $postType, $postID );
    }

    ?>

    <div class="bps-wrapper-grid-square-bottom-overlay-text-with-logo <?php echo $displayOptions['classes']; ?>">
        <div class="bps-item-image-div">
            <?php if( $imageUrl ) : ?>
                <div class="bps-item-image" style="background: url('<?php echo $imageUrl; ?>');">
                <?php if( $iconUrl ) : ?>
                    <div class="bps-item-icon-div">
                        <img class="bps-item-icon" src="<?php echo $iconUrl; ?>">
                    </div>
                <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="bps-item-data-div <?php echo $imageUrl ? '' : 'bps-item-no-image'; ?>">
            <a href="<?php echo get_permalink( $postID ); ?>">
                <h3 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h3>
            </a>
            <p class="bps-item-description"><?php echo get_the_excerpt( $postID ); ?></p>
            <a class="bps-item-button" href="<?php echo get_permalink( $postID ); ?>"><?php _e( 'Read more', 'beplustheme' ); ?></a>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>