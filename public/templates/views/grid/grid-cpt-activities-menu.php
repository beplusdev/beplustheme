<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;

    // Get the icon
    $iconUrl = bpsGetCptIconInTemplate( $postType, $postID );

    // Get all activities for this post
    $itemActivities = getAllItemActivities( get_post_type( $postID ), $postID )->post_count;		

    ?>

    <div class="bps-wrapper-grid-cpt-activities-menu <?php echo $displayOptions['classes']; ?>">
        <div class="bps-grid-cpt-activities-menu-item">
            <a href="<? echo get_permalink( $postID ); ?>">
                <h3 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h3>
            </a>
            <h6 class="bps-item-activities"><span class="activities-number"><?php echo $itemActivities; ?></span><?php _e( ' activities', 'beplustheme-child'); ?></h6>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>