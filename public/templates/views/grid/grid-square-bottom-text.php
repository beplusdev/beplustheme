<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;
    
    // Get the image
    $imageUrl = bpsGetCptImageInTemplate( $postType, $postID );

    // Get all categories in a string
    $categoriesArray = bpsGetCptTaxonomies( $postsOptions['post_taxonomy'], $postID );
    
    ?>

    <div class="bps-wrapper-grid-square-bottom-text <?php echo $displayOptions['classes']; ?>">
        <?php if( $imageUrl ) : ?>
            <div class="bps-item-image-div">
                <img class="bps-item-image" src="<?php echo $imageUrl; ?>">
            </div>
        <?php endif; ?>
        <div class="bps-item-data-div">
            <a href="<? echo get_permalink( $postID ); ?>">
                <h3 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h3>
            </a>
            <div class="bps-item-metadata-div">
                <p class="bps-item-metadata-date"><?php echo __( 'Published on ', 'beplustheme' ) . get_the_date( '', $postID ); ?></p>
                <?php if( !empty( $categoriesArray ) ) : ?>
                    <div class="bps-item-metadata-categories">
                        <?php foreach( $categoriesArray as $category ) : ?>
                            <p class="bps-item-metadata-category"><?php echo $category; ?></p>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <p class="bps-item-description"><?php echo get_the_excerpt( $postID ); ?></p>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>