<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;

    // Get the icon
    $iconUrl = bpsGetCptIconInTemplate( $postType, $postID );
    if( !$iconUrl ) {
        $iconUrl = bpsGetCptLogoInTemplate( $postType, $postID );
    }

    // Get all activities for this post
    $itemActivities = getAllItemActivities( get_post_type( $postID ), $postID )->post_count;		

    ?>

    <div class="bps-wrapper-grid-cpt-icons <?php echo $displayOptions['classes']; ?>">
        <div class="bps-grid-cpt-icons-item">
            <?php if( $iconUrl ) : ?>
                <div class="bps-item-icon-div">
                    <img class="bps-item-icon" src="<?php echo $iconUrl; ?>">
                </div>
            <?php endif; ?>
            <a href="<? echo get_permalink( $postID ); ?>">
                <h3 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h3>
            </a>
            <?php if( post_type_exists( 'activity' ) ) : ?>
                <h6 class="bps-item-activities"><?php printf( esc_html__( '%d activities', 'beplustheme-child'), $itemActivities ); ?></h6>
            <?php endif; ?>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>