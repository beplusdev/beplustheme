<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;
    
    // Get the image
    $imageUrl = bpsGetCptImageInTemplate( $postType, $postID );

    // Get the icon
    $iconUrl = bpsGetCptIconInTemplate( $postType, $postID );
    if( !$iconUrl ) {
        $iconUrl = bpsGetCptLogoInTemplate( $postType, $postID );
    }

    ?>

    <div class="bps-wrapper-grid-square-gradient-bg <?php echo $displayOptions['classes']; ?>" data-aos="fade-right">
    <?php if( $imageUrl ) : ?>
        <div class="grid-square-gradient-bg-image" style="background: linear-gradient( to top, #181c3a, rgba( 24, 28, 58, 0 ) ), url('<?php echo $imageUrl; ?>');">
    <?php else: ?>
        <div class="grid-square-gradient-bg-no-image" style="background: linear-gradient( to top, #181c3a, rgba( 24, 28, 58, 0 ) );">
    <?php endif; ?>
            <div class="bps-item-data-div">
                <?php if( $iconUrl ) : ?>
                    <div class="bps-item-icon-div">
                        <img class="bps-item-icon" src="<?php echo $iconUrl; ?>">
                    </div>
                <?php endif; ?>
                <a href="<? echo get_permalink( $postID ); ?>">
                    <h3 class="bps-item-title"><?php echo get_the_title( $postID ); ?></h3>
                </a>
                <p class="bps-item-description"><?php echo get_the_excerpt( $postID ); ?></p>
                <?php if( have_rows( 'linked_infrastructure_list', $postID ) ) : ?>
                    <?php while( have_rows( 'linked_infrastructure_list', $postID ) ) : ?>
                        <?php the_row(); ?>
                        <?php $infrastructureItem = get_sub_field( 'linked_infrastructure_list_infrastructure', $postID ); ?>
                        <p class="bps-item-linked-infrastructure">
                            <a class="linked-infrastructure-link" href="<?php echo get_permalink( $infrastructureItem->ID ); ?>">
                                <?php echo $infrastructureItem->post_title; ?>
                            </a>
                        </p>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>