<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$allDisplays = bpsDefineDisplayArray();

$postType = $postsOptions['post_type'];

/* Check if there's a CTA in the 1st position */
if( $firstCta = $ctaOptions['first_cta'] ) {

    if( in_array( $firstCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/'. $firstCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $firstCta );
    }

}

foreach( $queriedPosts as $post ) :

    $postID = $post->ID;
    
    // Get the image
    $imageUrl = bpsGetCptImageInTemplate( $postType, $postID );

    $sectionsArray = [
        $postType . '_first_flexible',
        $postType . '_second_flexible',
        $postType . '_third_flexible',
    ];

    /* Write here all the layouts we want to show in the contact cards */
    $layoutsToSearch = [ 
        'content',
        'name',
        'address',
        'phone_number',
        'mobile_number',
        'email_address',
        'website',
        'social',
    ];

    ?>

    <div class="bps-wrapper-grid-image-top-info-bottom <?php echo $displayOptions['classes']; ?>">
        <div class="grid-image-top-info-bottom-container">
            <div class="grid-image-top-info-bottom-image">
                <img class="bps-grid-image-top-info-bottom-image" src="<?php echo $imageUrl; ?>">
            </div>
            <div class="grid-image-top-info-bottom-content">
                <h4 class="grid-image-top-info-bottom-item-title"><?php echo get_the_title( $postID ); ?></h4>

                <?php foreach( $sectionsArray as $section ) : ?>
                    <?php if( have_rows( $section, $postID ) ) : ?>
                        <?php while( have_rows( $section, $postID ) ) : ?>
                            <?php the_row(); ?>
                            <?php if( in_array( get_row_layout(), $layoutsToSearch ) ) : ?>
                                <?php \BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() ); ?>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                <?php endforeach; ?>

            </div>
        </div>
    </div>

    <?php
        
endforeach;

/* Check if there's a CTA in the last position */
if( $lastCta = $ctaOptions['last_cta'] ) {

    if( in_array( $lastCta, $allDisplays ) ) {
        ob_start();
        loadTemplate( 
            'public/templates/views/cta/' . $lastCta .'.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            );
        echo ob_get_clean();
    } else {
        bpsMissingTemplateWarning( $lastCta );
    }

}

?>