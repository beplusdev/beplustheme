<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$postTypeToCheck = 'sport';
if( post_type_exists( $postTypeToCheck ) ) :

    global $post;

    $postType = $postsOptions['post_type'];

    ?>

    <div class="bps-wrapper-cta-all-sports <?php echo $displayOptions['classes']; ?>">
        <div class="card-block">
            <h3 class="bps-item-title">
                <?php _e( 'All sports', 'beplustheme' ); ?>
            </h3>
            <p class="bps-item-description"><?php _e( '+20 sports', 'beplustheme' ); ?></p>
            <a class="bps-item-link-button" href="<?php echo get_post_type_archive_link( $postTypeToCheck ); ?>"><?php _e( 'Discover', 'beplustheme' ); ?></a>
        </div>
    </div>

<?php else :

    bpsNeededPostTypeNotActiveWarning( $postTypeToCheck );

endif;

?>