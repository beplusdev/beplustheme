<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

    global $post;

    ?>

    <div class="bps-wrapper-cta-reservation <?php echo $displayOptions['classes']; ?>">
        <div class="card-block">
            <h3 class="bps-item-title">
                <?php _e( 'Book a field', 'beplustheme' ); ?>
            </h3>
            <p class="bps-item-description"><?php echo $ctaOptions['cta_description'] ?></p>
            <a class="bps-item-link-button <?php echo $ctaOptions['cta_popup_class'] ?>"><?php _e( 'Book', 'beplustheme' ); ?></a>
        </div>
    </div>
