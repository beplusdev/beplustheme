<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$postTypeToCheck = 'sport';
$filterPostSlug = $ctaOptions['filter_post_slug'];
if( post_type_exists( $postTypeToCheck ) && $filterPostSlug == 'sport' ) :

    $sportID = $ctaOptions['filter_post_id'];

    $postType = $postsOptions['post_type'];

    $iconUrl = bpsGetCptIcon( $postType );

    $sportActivities = getAllSportActivities( $sportID )->post_count;

    ?>

    <div class="bps-wrapper-cta-sport-icon <?php echo $displayOptions['classes']; ?>">
        <div class="bps-item-icon-div">
            <img class="bps-item-icon" src="<?php echo $iconUrl; ?>">
        </div>
        <h3 class="bps-item-title"><?php echo get_the_title( $sportID ); ?></h3>
        <h6 class="bps-item-activities"><?php printf( esc_html__( '%d activities', 'beplustheme-child'), $sportActivities ); ?></h6>
        <p class="bps-item-description"><?php echo get_the_excerpt( $sportID ); ?></p>
    </div>

<?php else :

    bpsNeededPostTypeNotActiveWarning( $postTypeToCheck );

endif; 

?>