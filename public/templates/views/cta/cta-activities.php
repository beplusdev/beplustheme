<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php

$postTypeToCheck = 'activity';
if( post_type_exists( $postTypeToCheck ) ) :

    global $post;

    $filterPostSlug = $ctaOptions['cta_slug'];
    $postID = $ctaOptions['cta_post_id'];
    
    $postTitle = get_the_title( $postID );

    $postType = $postsOptions['post_type'];

    ?>

    <div class="bps-wrapper-cta-club-activities <?php echo $displayOptions['classes']; ?>">
        <div class="card-block">
            <h3 class="bps-item-title">
                <a class="bps-item-next-arrow" href="../activity?<?php echo $filterPostSlug.'_id='.$postID; ?>">
                    <?php echo sprintf( __( 'Checkout all %s activities', 'beplustheme' ), $postTitle ); ?>
                    <span class="fa fa-arrow-right"></span>
                </a>
            </h3>
        </div>
    </div>

<?php else :

    bpsNeededPostTypeNotActiveWarning( $postTypeToCheck );

endif;

?>