<?php

/* Variables loaded: $queriedPosts, $displayOptions, $postsOptions, $ctaOptions */

?>

<?php if( ( $displayOptions['display'] == 'grid-square-bottom-overlay-text' ) ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-square-bottom-overlay-text.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'grid-square-bottom-overlay-text-with-logo' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-square-bottom-overlay-text-with-logo.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'grid-square-bottom-text' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-square-bottom-text.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'grid-square-gradient-bg' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-square-gradient-bg.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'grid-cpt-icons' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-cpt-icons.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'grid-cpt-activities-menu' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-cpt-activities-menu.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>
    
<?php elseif( $displayOptions['display'] == 'grid-image-top-info-bottom' ) : ?>
    
    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/grid/grid-image-top-info-bottom.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'card-overlay-text' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/card/card-overlay-text.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'card-activity' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/card/card-activity.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php elseif( $displayOptions['display'] == 'card-contact' ) : ?>

    <div class="container">
        <div class="row">

            <?php loadTemplate( 
            'public/templates/views/card/card-contact.php',
            [
                'queriedPosts' => $queriedPosts,
                'postsOptions' => $postsOptions,
                'displayOptions' => $displayOptions,
                'ctaOptions' => $ctaOptions,
                ]
            ); ?>

        </div>
    </div>

<?php else : ?>

    <?php bpsMissingTemplateWarning( $displayOptions['display'] ); ?>

<?php endif; ?>