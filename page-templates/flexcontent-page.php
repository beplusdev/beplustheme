<?php
/**
 * Template Name: Flexible Content Page
 *
 * Page template that uses ACF flexible contents
 *
 * @package beplustheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'beplustheme_container_type' );

/* Get sections fields */
$postType = get_post_type();

$flexible_first_section = $postType . '_first_flexible';
$flexible_second_section = $postType .'_second_flexible';
$flexible_third_section = $postType .'_third_flexible';
?>

<div class="wrapper" id="flex-content-page-wrapper">

    <!-- FIRST SECTION -->
    <div id="first-section-wrapper">
        <div id="first-section" style="<?php bps_the_first_section_style(); ?>">
            <div id="first-section-container" class="container first-section-info">

                <?php bps_the_content(); ?>
                
                <div class="flexible-items">
                    <?php
                    /* Check if the first section flexible content field has rows of data */
                    if ( have_rows( $flexible_first_section ) ):

                        /* Loop through the rows of data */
                        while ( have_rows( $flexible_first_section ) ) : the_row();

                            \BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() );

                        endwhile;

                    endif;
                    ?>
                </div>

            </div>
        </div>
    </div>
    <!-- END FIRST SECTION -->
    
    <!-- SECOND SECTION -->
    <div id="second-section-wrapper">
        <div id="second-section">
            <div id="second-section-container" class="container">

                <div class="flexible-items">
                    <?php
                        /* Check if the second section flexible content field has rows of data */
                        if ( have_rows( $flexible_second_section ) ):

                            /* Loop through the rows of data */
                            while ( have_rows( $flexible_second_section ) ) : the_row();

                                \BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() );

                            endwhile;

                        endif;
                    ?>
                </div>
                
            </div>
        </div>
    </div>
    <!-- END SECOND SECTION -->

    <!-- THIRD SECTION -->
    <div id="third-section-wrapper">
        <div id="third-section">
            <div id="third-section-container">

                <div class="flexible-items">
                        <?php
                        /* Check if the third section flexible content field has rows of data */
                        if ( have_rows( $flexible_third_section ) ):

                            /* Loop through the rows of data */
                            while ( have_rows( $flexible_third_section ) ) : the_row();

                                \BPS\BePlusPlugin\Helpers\BePlusPluginLayoutDisplay::displayLayout( get_row_layout() );				

                            endwhile;

                        endif;
                        ?>
                </div>
                
            </div>
        </div>
    </div>
    <!-- END THIRD SECTION -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>