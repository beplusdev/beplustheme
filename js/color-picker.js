/* Color picker */
(function( $ ) {
    $(function() {
         
        // Add Color Picker to all inputs that have 'beplustheme-color-picker' class
        $( '.beplustheme-color-picker' ).wpColorPicker();
         
    });
})( jQuery );