export default {
  methods: {
    translate(str) {
      if(vueTranslation.hasOwnProperty(str)) {
        return vueTranslation[str];
      } else {
        return str;
      }
    }
  }

}
