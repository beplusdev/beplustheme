import axios from 'axios';
const Qs = require('qs');
export default {

    getAjax: function( action, body ){

        let form_data = new FormData;
        form_data.append( 'action',  action );
        form_data.append( 'body', JSON.stringify( body ) );
        return axios.post( vueVars.ajaxUrl, form_data );

    },

    post: function( action, body ) {

        let form_data = new FormData;
        form_data.append( 'action', action );
        form_data.append( 'body', JSON.stringify( body ) );
        return axios.post( vueVars.ajaxUrl, form_data );

    }

}