import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import ActivitySearchForm from './components/ActivitySearchForm.vue';
import ActivitySearchFormHome from './components/ActivitySearchFormHome.vue';
Vue.use(BootstrapVue);

window.onload = function () {
  const asf = new Vue({
    el: '#activity-search-form',
    components: {
      ActivitySearchForm,
    }
  });
  const asfh = new Vue({
    el: '#activity-search-form-home',
    components: {
      ActivitySearchFormHome,
    }
  });
}